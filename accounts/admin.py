from django.contrib import admin
from django.core.exceptions import ValidationError
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User

from email_configurations.views import send_welcome_mail

class EmailRequiredMixin(object):
	def __init__(self, *args, **kwargs):
		super(EmailRequiredMixin, self).__init__(*args, **kwargs)
		# make user email field required
		self.fields['email'].required = True

class MyUserCreationForm(EmailRequiredMixin, UserCreationForm):
	pass


class MyUserChangeForm(EmailRequiredMixin, UserChangeForm):
	pass

class EmailRequiredUserAdmin(UserAdmin):
	form = MyUserChangeForm
	add_form = MyUserCreationForm
	add_fieldsets = ((None, { 
		'fields': ('username', 'email', 'password1', 'password2'), 
		'classes': ('wide',)
	}),)

	def clean(self):
		email = self.cleaned_data.get('email')
		username = self.cleaned_data.get('username')
		if email and User.objects.filter(email=email).exclude(username=username).exists():
			raise ValidationError(u'This Email address was already registered')
		return self.cleaned_data

	def save_model(self, request, obj, form, change):
		send_welcome_mail(user=obj.username, to_mail=obj.email)
		super(EmailRequiredUserAdmin, self).save_model(request, obj, form, change)


# Register your models here.

admin.site.unregister(User)
admin.site.register(User, EmailRequiredUserAdmin)