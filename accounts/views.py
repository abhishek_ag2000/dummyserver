import json
import urllib
from django.shortcuts import render, redirect
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash

from app.common import CommonMixin
from email_configurations.views import send_welcome_mail
from .forms import CreateUserForm
# Create your views here.


def signup_view(request):
	"""
	Signup View
	"""
	if request.method == 'POST':
		form = CreateUserForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data.get('username')
			email = form.cleaned_data.get('email')
			password = form.cleaned_data.get('password1')
			form.save()
			# send_welcome_mail(user=username,to_mail=email)
			return redirect('accounts:login')
		else:
			messages.error(request, 'Please Correct The Error Below.')
	else:
		form = CreateUserForm()

	context = {

		'form': form,
	}

	return CommonMixin.render(request, 'signup.html', context)


class CustomLoginView(CommonMixin, LoginView):
    """
    Custom Auth Login View To pass extra context
    """
    template_name = "login.html"


@login_required
def change_password_view(request):
	"""
	Change Password View
	"""
	if request.method == 'POST':
		form = PasswordChangeForm(request.user, request.POST)
		if form.is_valid():
			user = form.save()
			update_session_auth_hash(request, user)  # Important!
			messages.success(
				request, 'Your password was successfully updated!')
			return redirect('user_profile:profile_details')

		messages.error(request, 'Please correct the error below.')
	else:
		form = PasswordChangeForm(request.user)

	context = {
		'form': form,
	}

	return render(request, 'change_password.html', context)