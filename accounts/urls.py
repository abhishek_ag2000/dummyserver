"""
Arka (URLs)
"""
from django.conf.urls import url
from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'accounts'

urlpatterns = [
	
	url(r"signup/$", views.signup_view, name="signup"),
	url(r'login/$', views.CustomLoginView.as_view(), name="login"),
	url(r"logout/$", auth_views.LogoutView.as_view(), name="logout"),

	#change password inside the ecosystem
	
    url(r'^change_password/$', views.change_password_view, name="change_password"),


]