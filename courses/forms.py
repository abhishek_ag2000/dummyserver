"""
Forms
"""
from django import forms

from .models import CourseReview, CourseQuestion, UserCourses, Questions, Answer, UserQuiz

class DateInput(forms.DateInput):
    """
    Widgets support for date input
    """
    input_type = 'date'


class CourseReviewForm(forms.ModelForm):
    """
    CourseReview Form
    """

    def __init__(self, *args, **kwargs):
        super(CourseReviewForm, self).__init__(*args, **kwargs)
        self.fields['review'].widget.attrs = {'class': 'form-control', }

    class Meta:
        model = CourseReview
        fields = ('review',)


class CourseQuestionForm(forms.ModelForm):
    """
    CourseQuestion Form
    """

    def __init__(self, *args, **kwargs):
        super(CourseQuestionForm, self).__init__(*args, **kwargs)
        self.fields['question'].widget.attrs = {'class': 'form-control', }

    class Meta:
        model = CourseQuestion
        fields = ('question',)

class UserCoursesForm(forms.ModelForm):
    """
    UserCourses Form
    """

    def __init__(self, *args, **kwargs):
        super(UserCoursesForm, self).__init__(*args, **kwargs)
        self.fields['date_of_expiry'].widget.attrs = {'class': 'form-control', }
    

    class Meta:
        model = UserCourses
        fields = ('date_of_expiry',)
        widgets = {
            'date_of_expiry': DateInput(),
        }


class UserQuizForm(forms.ModelForm):
    """
    User Quiz Form
    """ 
    class Meta:
        model = UserQuiz
        fields = ('answer',)

    def __init__(self, *args, **kwargs):
        self.question = kwargs.pop('question', None)

        super(UserQuizForm, self).__init__(*args, **kwargs)


        self.fields['answer'] = forms.ModelChoiceField(queryset=Answer.objects.filter(question=self.question), widget=forms.RadioSelect, empty_label=None)

