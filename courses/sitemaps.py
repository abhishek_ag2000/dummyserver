from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse

from .models import Courses, CourseSubCategory, CourseCategory

class AllCourseCategorySiteMap(Sitemap):
	"""
	SiteMap For All Blogs
	"""
	changefreq = "always"
	priority = 0.7

	def items(self):
		return CourseCategory.objects.all()

	def lastmod(self, obj):
		return obj.name


class AllCourseSubCategorySiteMap(Sitemap):
	"""
	SiteMap For All Blogs
	"""
	changefreq = "always"
	priority = 0.4

	def items(self):
		return CourseSubCategory.objects.all()

	def lastmod(self, obj):
		return obj.name

class AllCoursesSiteMap(Sitemap):
	"""
	SiteMap For All Blog Categories
	"""
	changefreq = "always"
	priority = 0.2

	def items(self):
		return Courses.objects.all()

	def lastmod(self, obj):
		return obj.name