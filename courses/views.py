import zipfile
import os
from io import BytesIO
import decimal
import datetime
from datetime import date
from dateutil.relativedelta import relativedelta

from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView
from django.views.decorators.cache import cache_page
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Sum, Value, Count, Avg, Q
from django.db.models.functions import Coalesce
from django.urls import reverse
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages

from about_us.models import AboutUs
from app.common import CommonMixin, CommonAdminMixin
from app.models import CertificateSignatures, Notification, CourseFeedback
from ecommerce_cart.models import DemoItems, DemoPackageItems
from ecommerce_cart.extras import generate_certificate_no
from email_configurations.views import course_completion_mail, send_course_question_mail
from user_profile.models import Profile

from .forms import CourseReviewForm, CourseQuestionForm, UserCoursesForm, UserQuizForm
from .models import CourseCategory, CourseSubCategory, Courses, CourseContent, CourseFilesAttached, CourseReview, UserCourses, CourseContentViewedUser, CourseQuestion, Questions, Answer, UserQuiz, CoursesPackages, CourseElements, SkillsTrainingVideos
# Create your views here.

@cache_page(60 * 15)
def course_category_details_view(request, category_slug):
	"""
	Course Category Detail View
	"""
	course_category_details = CourseCategory.objects.filter(slug=category_slug).first()

	sub_category_list = CourseSubCategory.objects.filter(category=course_category_details).order_by('id')

	context = {
		'course_category_details' : course_category_details,
		'sub_category_list' : sub_category_list
	}
	return CommonMixin.render(request, 'course_category_details.html', context)


@cache_page(60 * 15)
def course_sub_category_details_view(request, sub_category_slug):
	"""
	Course Sub Category Detail View
	"""
	course_sub_category_details = CourseSubCategory.objects.filter(slug=sub_category_slug).first()

	courses = Courses.objects.filter(sub_category=course_sub_category_details, show_course=True).order_by('id')

	page = request.GET.get('page', 1)

	paginator = Paginator(courses, 9)
	try:
		course_list = paginator.page(page)
	except PageNotAnInteger:
		course_list = paginator.page(1)
	except EmptyPage:
		course_list = paginator.page(paginator.num_pages)

	package_list = CoursesPackages.objects.filter(sub_category=course_sub_category_details).order_by('id')

	context = {
		'course_sub_category_details' : course_sub_category_details,
		'course_list' : course_list,
		'package_list' : package_list
	}
	return CommonMixin.render(request, 'course_sub_category_details.html', context)


@cache_page(60 * 15)
def course_details_view(request, course_slug):
	"""
	Course Details View
	"""
	category_list = CourseCategory.objects.all().order_by('name')

	course_details = Courses.objects.filter(slug=course_slug).first()

	course_content = CourseContent.objects.filter(course=course_details).order_by('id')

	related_courses = Courses.objects.filter(sub_category=course_details.sub_category, show_course=True).exclude(id=course_details.id).order_by('id')[:5]

	course_reviews = CourseReview.objects.filter(course=course_details).order_by('id')

	if request.user.is_authenticated:
		applied_course = UserCourses.objects.filter(user=request.user, course=course_details).first()

		cart_course = DemoItems.objects.filter(user=request.user, course=course_details).first()
	else:
		applied_course = None

		cart_course = None

	review_avg = CourseReview.objects.filter(course=course_details).aggregate(the_avg=Coalesce(Avg('rating'), Value(0)))['the_avg']

	review_list = CourseFeedback.objects.all().order_by('id')

	video_list = SkillsTrainingVideos.objects.all().order_by('id')

	context = {
		'category_list' : category_list,
		'course_details' : course_details,
		'course_content' : course_content,
		'related_courses' : related_courses,
		'course_reviews' : course_reviews,
		'applied_course' : applied_course,
		'cart_course' : cart_course,
		'review_avg' : review_avg,
		'review_list' : review_list,
		'video_list' : video_list
	}
	return CommonMixin.render(request, 'course_details.html', context)

@login_required
@cache_page(60 * 15)
def all_course_list(request):
	"""
	All Course List
	"""
	if not request.user.is_superuser:
		messages.error(request, 'You have no permission to access the requested resource!')
		return redirect(reverse('index_student'))


	courses = Courses.objects.all().order_by('id')

	page = request.GET.get('page', 1)

	paginator = Paginator(courses, 9)
	try:
		course_list = paginator.page(page)
	except PageNotAnInteger:
		course_list = paginator.page(1)
	except EmptyPage:
		course_list = paginator.page(paginator.num_pages)

	context = {
		'course_list' : course_list
	}
	return CommonAdminMixin.render(request, 'all_course.html', context)


@cache_page(60 * 15)
def skills_list(request):
	"""
	Skills List View
	"""
	category_list = CourseCategory.objects.all().order_by('name')

	course_elements = CourseElements.objects.filter(sample__exact='COURSE-ELEMENTS').first()

	review_list = CourseFeedback.objects.all().order_by('id')

	video_list = SkillsTrainingVideos.objects.all().order_by('id')

	about = AboutUs.objects.filter(sample__exact='About-Us').first()

	context = {
		'about' : about,
		'category_list' : category_list,
		'course_elements' : course_elements,
		'review_list' : review_list,
		'video_list' : video_list
	}
	return CommonMixin.render(request, 'skills.html', context)

@login_required
def my_course_list_view(request):
	"""
	My Course List
	"""
	courses = UserCourses.objects.filter(user=request.user).order_by('id')

	page = request.GET.get('page', 1)

	paginator = Paginator(courses, 9)
	try:
		course_list = paginator.page(page)
	except PageNotAnInteger:
		course_list = paginator.page(1)
	except EmptyPage:
		course_list = paginator.page(paginator.num_pages)

	if request.user.is_authenticated:
		#### Courses Removal on expiry

		# Iterate through them
		for item in courses:
			if item.date_of_expiry < timezone.now():

				# If the expiration date is bigger than first remove user from course user list
				item.course.subscribed_users.remove(item.user)
				item.course.save()

				# Then Delete the User Course Model
				item.delete()

	context = {
		'course_list' : course_list
	}
	return CommonAdminMixin.render(request, 'my_course.html', context)

@login_required
@cache_page(60 * 15)
def admin_course_details(request, course_slug):
	"""
	My Course Details
	"""
	if not request.user.is_superuser:
		messages.error(request, 'You have no permission to access the requested resource!')
		return redirect(reverse('index_student'))


	course_details = Courses.objects.filter(slug=course_slug).first()

	course_content = CourseContent.objects.filter(course=course_details).order_by('id')

	course_content_completed = CourseContentViewedUser.objects.filter(user=request.user,course=course_details, completed=True).order_by('id')

	course_content_total = course_content.aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Course Content

	course_content_completed_total = course_content_completed.aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Course Content Completed

	if course_content:
		percentage_completed = (course_content_completed_total / course_content_total) * 100
	else:
		percentage_completed = 0

	applied_course = UserCourses.objects.filter(user=request.user, course=course_details).first()

	review_avg = CourseReview.objects.filter(course=course_details).aggregate(the_avg=Coalesce(Avg('rating'), Value(0)))['the_avg']

	if review_avg:

		rating_avg = range(int(review_avg))
		empty_avg_star = range(int(5)-int(review_avg))

	else:

		rating_avg = range(int(0))
		empty_avg_star = range(int(5)-int(0))

			

	context = {
		'course_details' : course_details,
		'course_content' : course_content,
		'applied_course' : applied_course,
		'percentage_completed' : percentage_completed,
		'rating_avg' : rating_avg,
		'empty_avg_star' : empty_avg_star,
		'review_avg' : review_avg
	}
	return CommonAdminMixin.render(request, 'admin_course_details.html', context)

@login_required
def my_course_details(request, course_slug):
	"""
	My Course Details
	"""
	course_details = Courses.objects.filter(slug=course_slug).first()

	course_content = CourseContent.objects.filter(course=course_details).order_by('id')

	course_content_completed = CourseContentViewedUser.objects.filter(user=request.user,course=course_details, completed=True).order_by('id')

	course_content_total = course_content.aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Course Content

	course_content_completed_total = course_content_completed.aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Course Content Completed

	if course_content:
		percentage_completed = (course_content_completed_total / course_content_total) * 100
	else:
		percentage_completed = 0

	applied_course = UserCourses.objects.filter(user=request.user, course=course_details).first()

	if request.method == 'POST' and 'btnform1' in request.POST:
		review_form = CourseReviewForm(request.POST or None)
		if review_form.is_valid():
			rating = request.POST.get('rating')
			review = request.POST.get('review')
			review = CourseReview.objects.create(user=request.user, review=review, rating=rating, course=course_details)
			review.save()
			messages.success(
                        request, 'Your Review is submitted successfully')
			return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
	else:
		review_form = CourseReviewForm()

	review_avg = CourseReview.objects.filter(course=course_details).aggregate(the_avg=Coalesce(Avg('rating'), Value(0)))['the_avg']

	if review_avg:

		rating_avg = range(int(review_avg))
		empty_avg_star = range(int(5)-int(review_avg))

	else:

		rating_avg = range(int(0))
		empty_avg_star = range(int(5)-int(0))

	if request.method == 'POST' and 'btnform2' in request.POST:
		question_form = CourseQuestionForm(request.POST or None)
		if question_form.is_valid():
			question = request.POST.get('question')
			review = CourseQuestion.objects.create(user=request.user, question=question, course=course_details)
			review.save()
			notification_text = question + ' is asked by you for course ' + course_details.name
			ns = Notification.objects.create(user=request.user, notification_for=course_details.name,text=notification_text)
			ns.save()
			# send_course_question_mail(contact=review)
			messages.success(request, 'Your Question is forwarded! Our faculty will get to you soon!')
			return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
	else:
		question_form = CourseQuestionForm()

	user_subscribed_course = UserCourses.objects.filter(user=request.user, course=course_details).first()

	if percentage_completed == 100 and not user_subscribed_course.completion_mail == True:
		# course_completion_mail(user=request.user, course=course_details, to_mail=request.user.profile.email)
		notification_text = course_details.name + 'is completed by you'
		ns = Notification.objects.create(user=request.user, notification_for=course_details.name,text=notification_text)
		ns.save()
		user_subscribed_course.completion_mail = True
		user_subscribed_course.save()

	unlock_certificate = UserCourses.objects.filter(user=request.user, course=course_details, is_certificate_unlocked=True).first()
			

	context = {
		'course_details' : course_details,
		'course_content' : course_content,
		'applied_course' : applied_course,
		'percentage_completed' : percentage_completed,
		'form' : review_form,
		'question_form' : question_form,
		'rating_avg' : rating_avg,
		'empty_avg_star' : empty_avg_star,
		'review_avg' : review_avg,
		'unlock_certificate' : unlock_certificate,
	}
	return CommonAdminMixin.render(request, 'my_course_details.html', context)

@login_required
def my_course_content_details(request, course_content_pk):
	"""
	My Course Content Details
	"""
	course_content_details = CourseContent.objects.filter(pk=course_content_pk).first()

	course_content = CourseContent.objects.filter(course=course_content_details.course).order_by('id')

	course_content_completed = CourseContentViewedUser.objects.filter(user=request.user,course=course_content_details.course, completed=True).order_by('id')

	course_content_total = course_content.aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Course Content

	course_content_completed_total = course_content_completed.aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Course Content Completed

	if course_content:
		percentage_completed = (course_content_completed_total / course_content_total) * 100
	else:
		percentage_completed = 0

	content_viewed = CourseContentViewedUser.objects.filter(user=request.user,course_content=course_content_details, course=course_content_details.course, completed=True).first()

	if not content_viewed:
		viewed_content = CourseContentViewedUser.objects.create(user=request.user,course_content=course_content_details, course=course_content_details.course, completed=True)
		viewed_content.save()

	context = {
		'course_content_details' : course_content_details,
		'course_content' : course_content,
		'percentage_completed' : percentage_completed
	}
	return CommonAdminMixin.render(request, 'course_content_details.html', context)

@login_required
def download_zip_files(request, course_pk):
	"""
	Download Attached Zip Files
	"""
	course_details = Courses.objects.filter(pk=course_pk).first()

	file_list = CourseFilesAttached.objects.filter(course=course_details)

	byte_data = BytesIO()

	zip_file = zipfile.ZipFile(byte_data, "w")

	for item in file_list:
		filename = os.path.basename(os.path.normpath(item.file.path))
		zip_file.write(item.file.path, filename)
	zip_file.close()

	response = HttpResponse(byte_data.getvalue(), content_type='application/zip')

	response['Content-Disposition'] = 'attachment; filename=files.zip'

	zip_file.printdir()

	return response


@login_required
@cache_page(60 * 15)
def user_courses_list(request, profile_pk):
	"""
	User Courses List
	"""
	if not request.user.is_superuser:
		messages.error(request, 'You have no permission to access the requested resource!')
		return redirect(reverse('index_student'))
		
	profile_details = Profile.objects.filter(pk=profile_pk).first()

	user_courses = UserCourses.objects.filter(user=profile_details.user).order_by('-id')

	total_orders = user_courses.aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Course Content

	total_earning = user_courses.aggregate(
            the_sum=Coalesce(Sum('total'), Value(0)))['the_sum'] # Total Course Content

	average_earning = user_courses.aggregate(
            the_sum=Coalesce(Avg('total'), Value(0)))['the_sum'] # Total Course Content

	browser_family = ['Mobile Safari', 'Safari']

	context = {
		'total_orders' : total_orders,
		'total_earning' : total_earning,
		'average_earning' : average_earning,
		'user_courses' : user_courses,
		'profile_details' : profile_details,
		'browser_family' : browser_family
	}
	return CommonAdminMixin.render(request, 'user_course_list.html', context)


def save_course(request, profile_pk, form, template_name):

	profile_details = Profile.objects.filter(pk=profile_pk).first()

	browser_family = ['Mobile Safari', 'Safari']

	data = {'is_error': False, 'error_message': ""}

	if not request.user.is_authenticated:
		data['is_error'] = True
		data['error_message'] = "Login Failed!"
		return JsonResponse(data)

	data = dict()
	if request.method == 'POST':
		if form.is_valid():
			form.save()
			data['form_is_valid'] = True
			queries = UserCourses.objects.filter(user=profile_details.user).order_by('-id')
			data['query'] = render_to_string(
	    		'user_course_list_ajax.html', {'queries': queries,'profile_details' : profile_details, 'browser_family':browser_family})
		else:
			data['is_error'] = True
			data['error_message'] = "Please rectify the form error and then try again"
	else:
		data['form_is_valid'] = False

	context = {
		'form': form,
		'profile_details' : profile_details,
		'browser_family' : browser_family
		}

	data['html_form'] = render_to_string(
		template_name, context, request=request)

	return JsonResponse(data)

def course_update_ajax(request, profile_pk, course_pk):
	"""
	Ajax request to change Course Validity Date
	"""
	query_details = UserCourses.objects.filter(pk=course_pk).first()

	if request.method == 'POST':
		form = UserCoursesForm(request.POST, instance=query_details)
	else:
		form = UserCoursesForm(instance=query_details)

	return save_course(request, profile_pk, form, 'course_update.html')

def course_completion_certificate(request, course_slug):
	"""
	Course Completion Certificate
	"""
	profile_details = Profile.objects.filter(user=request.user).first()

	course_details = Courses.objects.filter(slug=course_slug).first()

	if not profile_details.full_name:
		messages.error(request, 'You need to enter your full name in profile edit, before generating certificate')
		return redirect(reverse('courses:my_course_details', kwargs={'course_slug': course_details.slug}))

	user_course = UserCourses.objects.filter(user=request.user, course=course_details).first()

	if not user_course.certificate_no:
		user_course.certificate_no = generate_certificate_no()
		user_course.save()

	signatures = CertificateSignatures.objects.filter(sample__exact='CERTIFICATE-SIGNATURES').first()
	

	context = {
		'course_details' : course_details,
		'user_course' : user_course,
		'signatures' : signatures
	}
	return CommonAdminMixin.render(request, 'certificate.html', context)

def course_for_quiz_list_view(request):
	"""
	Course For Quiz List View
	"""
	courses = UserCourses.objects.filter(user=request.user).order_by('id')


	page = request.GET.get('page', 1)

	paginator = Paginator(courses, 9)
	try:
		course_list = paginator.page(page)
	except PageNotAnInteger:
		course_list = paginator.page(1)
	except EmptyPage:
		course_list = paginator.page(paginator.num_pages)

	context = {
		'course_list' : course_list
	}
	return CommonAdminMixin.render(request, 'quiz_courses.html', context)


def create_user_quiz(request, course_slug, question_order):
	"""
	Create quiz Order
	"""
	course_details = Courses.objects.filter(slug=course_slug).first()

	question_details = Questions.objects.filter(course=course_details, order=question_order).first()

	questions_count = Questions.objects.filter(course=course_details).aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum']

	counter = question_details.order + 1

	if counter > questions_count:
		counter = 0

	if request.method == "POST":
		form = UserQuizForm(request.POST, question=question_details)
		if form.is_valid():
			answer = Answer.objects.get(id=request.POST.get('answer'))
			quiz = UserQuiz.objects.create(user=request.user, course=course_details, question=question_details, answer=answer)
			quiz.save()
			if counter != 0:
				return HttpResponseRedirect(reverse('courses:create_user_quiz', kwargs={'course_slug': course_details.slug, 'question_order' : counter}))
			else:
				return HttpResponseRedirect(reverse('courses:quiz_result', kwargs={'course_slug': course_details.slug}))
	else:
		form = UserQuizForm(question=question_details)

	context = {
		'form' : form,
		'course_details' : course_details,
		'question_details' : question_details,
		'questions_count' : questions_count
	}
	return CommonAdminMixin.render(request, 'create_quiz.html', context)


def quiz_result_view(request, course_slug):
	"""
	Quiz Result View
	"""
	course_details = Courses.objects.filter(slug=course_slug).first()

	total_questions = Questions.objects.filter(course=course_details).aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Course Questions

	right_answer_count = UserQuiz.objects.filter(user=request.user, course=course_details, answer__is_correct=True).aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Correct Answer

	percentage = (right_answer_count / total_questions) * 100

	context = {
		'course_details' : course_details,
		'total_questions' : total_questions,
		'right_answer_count' : right_answer_count,
		'percentage' : percentage
	}
	return CommonAdminMixin.render(request, 'quiz_result.html', context)

def restart_quiz(request, course_slug):
	"""
	Restart Quiz
	"""
	course_details = Courses.objects.filter(slug=course_slug).first()

	user_quiz = UserQuiz.objects.filter(user=request.user, course=course_details)

	for item in user_quiz:
		item.delete()

	messages.success(request, 'Your Quiz is Restarted successfully')

	return HttpResponseRedirect(reverse('courses:create_user_quiz', kwargs={'course_slug': course_details.slug, 'question_order' : 1}))


def all_users_answer(request, course_slug):
	"""
	All User Answer View
	"""
	course_details = Courses.objects.filter(slug=course_slug).first()

	user_answers = UserQuiz.objects.filter(user=request.user, course=course_details).order_by('question__order')

	context = {
		'course_details' : course_details,
		'user_answers' : user_answers
	}
	return CommonAdminMixin.render(request, 'user_quiz_answers.html', context)

class PackageListView(CommonMixin, ListView):
	"""
	Package List View
	"""
	model = CoursesPackages
	context_object_name = 'course_package_list'
	template_name = 'package_list.html'
	paginate_by = 4

	def get_queryset(self):
		return self.model.objects.all().order_by('-id')

class PackageDetailView(CommonMixin, DetailView):
	"""
	Package Details View
	"""
	context_object_name = 'package_details'
	model = CoursesPackages
	template_name = 'package_details.html'

	def get_object(self):
		package_details = CoursesPackages.objects.filter(slug=self.kwargs['package_slug']).first()
		return package_details

	def get_context_data(self, **kwargs):
		context = super(PackageDetailView, self).get_context_data(**kwargs)

		package_details = self.get_object()

		context['course_list'] = package_details.courses.all()

		if self.request.user.is_authenticated:
			context['cart_course'] = DemoPackageItems.objects.filter(user=self.request.user, packages=package_details).first()
		else:
			context['cart_course'] = DemoPackageItems.objects.filter(packages=package_details).first()

		return context

def search_courses(request):
	"""
	Universal Search Courses
	"""
	template = 'search_courses.html'

	query = request.GET.get('q')

	if query:
		result = Courses.objects.filter(Q(sub_category__name__icontains=query) | Q(
            description__icontains=query) | Q(name__icontains=query))
	else:
		result = Courses.objects.none()

	page = request.GET.get('page', 1)

	paginator = Paginator(result, 9)
	try:
		course_list = paginator.page(page)
	except PageNotAnInteger:
		course_list = paginator.page(1)
	except EmptyPage:
		course_list = paginator.page(paginator.num_pages)

	context = {
		'search_result' : query,
		'course_list' : course_list
	}
	return CommonMixin.render(request, template, context)


def search_admin_courses(request):
	"""
	Search Courses For Admin
	"""
	template = 'search_admin_courses.html'

	query = request.GET.get('q')

	if query:
		result = Courses.objects.filter(Q(sub_category__name__icontains=query) | Q(
            description__icontains=query) | Q(name__icontains=query))
	else:
		result = Courses.objects.none()

	page = request.GET.get('page', 1)

	paginator = Paginator(result, 9)
	try:
		course_list = paginator.page(page)
	except PageNotAnInteger:
		course_list = paginator.page(1)
	except EmptyPage:
		course_list = paginator.page(paginator.num_pages)

	context = {
		'search_result' : query,
		'course_list' : course_list
	}
	return CommonAdminMixin.render(request, template, context)


def search_student_courses(request):
	"""
	Search Courses For Student
	"""
	template = 'search_student_courses.html'

	query = request.GET.get('q')

	if query:
		result = UserCourses.objects.filter(Q(user=request.user),Q(course__sub_category__name__icontains=query) | Q(
            course__description__icontains=query) | Q(course__name__icontains=query))
	else:
		result = UserCourses.objects.none()

	page = request.GET.get('page', 1)

	paginator = Paginator(result, 9)
	try:
		course_list = paginator.page(page)
	except PageNotAnInteger:
		course_list = paginator.page(1)
	except EmptyPage:
		course_list = paginator.page(paginator.num_pages)

	context = {
		'search_result' : query,
		'course_list' : course_list
	}
	return CommonAdminMixin.render(request, template, context)


@login_required
def user_course_manual_subscription_list(request, profile_pk):
	"""
	View TO Get All the courses manually subscribed by Admin
	"""
	if not request.user.is_superuser:
		messages.error(request, 'You have no permission to access the requested resource!')
		return redirect(reverse('index_student'))


	profile_details = Profile.objects.filter(pk=profile_pk).first()

	courses = Courses.objects.all().order_by('id')

	page = request.GET.get('page', 1)

	paginator = Paginator(courses, 9)
	try:
		course_list = paginator.page(page)
	except PageNotAnInteger:
		course_list = paginator.page(1)
	except EmptyPage:
		course_list = paginator.page(paginator.num_pages)

	context = {
		'profile_details' : profile_details,
		'course_list' :  course_list
	}
	return CommonAdminMixin.render(request, 'user_admin_courses.html', context)


@login_required
def add_course_to_student(request, course_pk, profile_pk):
	"""
	Add Course To Student
	"""
	if not request.user.is_superuser:
		messages.error(request, 'You have no permission to access the requested resource!')
		return redirect(reverse('index_student'))


	profile_details = Profile.objects.filter(pk=profile_pk).first()

	course_details = Courses.objects.filter(pk=course_pk).first()

	user_course_details = UserCourses.objects.filter(user=profile_details.user, course=course_details).first()

	if user_course_details:
		messages.error(request, 'This course is already added for user ' + str(profile_details.user))
		return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

	expiry_month = course_details.course_valid_till_months
	date_of_expiry = date.today() + relativedelta(months=+expiry_month)

	course_details.subscribed_users.add(profile_details.user)
	course_details.save()

	user_course = UserCourses.objects.create(user=profile_details.user, course=course_details,
												date_of_expiry=date_of_expiry, total=0, subscription_type='By ADMIN')
	user_course.save()

	messages.success(request, 'Course Added Successfully to user ' + profile_details.user.username)

	return HttpResponseRedirect(reverse('courses:user_courses_list', kwargs={'profile_pk': profile_details.pk}))

	
def unlock_certificate(request, user_courses_pk):
	"""
	Unlock Certificate by button click
	"""
	user_course = UserCourses.objects.filter(pk=user_courses_pk).first()

	user_course.is_certificate_unlocked = True
	user_course.save()

	messages.success(request, 'Certificate Unlocked For Course ' + user_course.course.name + ' For User ' + user_course.user.username)

	return HttpResponseRedirect(request.META.get("HTTP_REFERER"))