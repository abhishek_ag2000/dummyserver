import os
import sys
from PIL import Image
from io import BytesIO
from datetime import date
from dateutil.relativedelta import relativedelta

from django.db import models
from django.conf import settings
from django.db.models import Sum, Value, Count, Max
from django.urls import reverse
from django.core.exceptions import ValidationError
from django.utils.functional import cached_property
from django.db.models.functions import Coalesce
from django.utils.text import slugify
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.validators import URLValidator
from embed_video.fields import EmbedVideoField
from django.core.files.uploadedfile import InMemoryUploadedFile



def file_size(value):  # add this to some file where you can import it from
	"""
	Function to validate file size
	"""
	MAX_UPLOAD_SIZE = 20971520
	if value.size > MAX_UPLOAD_SIZE:
		raise ValidationError('File too large. Size should not exceed 20 MB.')

		
class CourseCategory(models.Model):
	"""
	Course Category Model
	"""
	name = models.CharField(max_length=100)
	slug = models.SlugField(
        max_length=200, unique=True, null=True, blank=True)
	ordering_id = models.IntegerField(default=0)

	class Meta:
		app_label = 'courses'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(CourseCategory, self).save(*args, **kwargs)

	def get_absolute_url(self):
		"""
		Returns absolute url for a course categories
		"""
		return reverse("courses:course_category_details", kwargs={'category_slug': self.slug})

	@cached_property
	def get_courses(self):
		"""
		Function to get couses present under selected category
		"""
		return Courses.objects.filter(sub_category__category=self.pk, show_on_skill_page=True).order_by('-id')[:10]

	@cached_property
	def get_courses_count(self):
		"""
		Function to get couses count
		"""
		course_list = Courses.objects.filter(sub_category__category=self.pk).order_by('name')

		course_count = course_list.aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Courses

		return course_count

class CourseSubCategory(models.Model):
	"""
	Course Sub Category Model
	"""
	name = models.CharField(max_length=200)
	sub_heading_text = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	category = models.ForeignKey(
        CourseCategory, on_delete=models.CASCADE, related_name='course_category', null=True)
	slug = models.SlugField(
        max_length=200, unique=True, null=True, blank=True)

	class Meta:
		app_label = 'courses'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(CourseSubCategory, self).save(*args, **kwargs)

	def get_absolute_url(self):
		"""
		Returns absolute url for a course sub categories
		"""
		return reverse("courses:course_sub_category_details", kwargs={'sub_category_slug': self.slug})



# Create your models here.
class Courses(models.Model):
	"""
	Courses Model
	"""
	name = models.CharField(max_length=200)
	sub_category = models.ForeignKey(
        CourseSubCategory, on_delete=models.CASCADE, related_name='course_sub_category', null=True)
	description = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	course_starts = models.CharField(max_length=255, blank=True, null=True)
	upload_course_brochure = models.FileField(upload_to="course_files/%Y/%m/%d", validators=[file_size], blank=True, null=True)
	course_valid_till_months = models.IntegerField(null=True)
	duration = models.CharField(max_length=200)
	course_price = models.DecimalField(default=0.00, max_digits=20, decimal_places=2)
	discount = models.DecimalField(default=0.00, max_digits=20, decimal_places=2)
	course_total = models.DecimalField(default=0.00, max_digits=20, decimal_places=2)
	mode_of_delivery = models.CharField(max_length=200)
	action_text = models.CharField(max_length=200,blank=True,null=True)
	image = models.ImageField(
		upload_to='course_image', null=True, blank=True, help_text='Image Size 829 x 400')
	course_video_link = EmbedVideoField(blank=True, null=True)  # same like models.URLField()
	show_on_skill_page = models.BooleanField(default=False)
	show_on_landing_page = models.BooleanField(default=False)
	show_course = models.BooleanField(default=False)
	slug = models.SlugField(
        max_length=200, unique=True, null=True, blank=True)
	subscribed_users = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name='courses_user', blank=True)
	course_content_heading_text = RichTextUploadingField(
		blank=True, null=True, config_name='special')


	class Meta:
		app_label = 'courses'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)

		if self.discount:
			disc_value = (self.course_price * self.discount) / 100
			self.course_total = self.course_price - disc_value
		else:
			self.course_total = self.course_price

		if self.image:
			temp_image = Image.open(self.image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((829, 400))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)
		super(Courses, self).save(*args, **kwargs)

	def get_absolute_url(self):
		"""
		Returns absolute url for a course 
		"""
		return reverse("courses:course_details", kwargs={'course_slug': self.slug})


class CoursesPackages(models.Model):
	"""
	Course Packages Model
	"""
	package_name = models.CharField(max_length=200)
	sub_category = models.ForeignKey(
        CourseSubCategory, on_delete=models.CASCADE, related_name='package_sub_category', null=True)
	courses = models.ManyToManyField(
        Courses, related_name='courses_package', blank=True)
	description = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	package_price = models.DecimalField(default=0.00, max_digits=20, decimal_places=2)
	discount = models.DecimalField(default=0.00, max_digits=20, decimal_places=2)
	package_total = models.DecimalField(default=0.00, max_digits=20, decimal_places=2)
	image = models.ImageField(
		upload_to='package_image', null=True, blank=True, help_text='Image Size 829 x 400')
	slug = models.SlugField(
        max_length=200, unique=True, null=True, blank=True)
	subscribed_users = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name='courses_package_user', blank=True)

	class Meta:
		app_label = 'courses'

	def __str__(self):
		return self.package_name

	def save(self, *args, **kwargs):
		self.slug = slugify(self.package_name)

		if self.discount:
			disc_value = (self.package_price * self.discount) / 100
			self.package_total = self.package_price - disc_value
		else:
			self.package_total = self.package_price

		if self.image:
			temp_image = Image.open(self.image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((829, 400))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)
		super(CoursesPackages, self).save(*args, **kwargs)




class CourseContent(models.Model):
	"""
	Courses Content Model
	"""
	course = models.ForeignKey(
		Courses, on_delete=models.CASCADE, related_name='course_contents', null=True)
	name = models.CharField(max_length=200)
	duration_in_minutes = models.IntegerField(default=0)
	duration_in_seconds = models.IntegerField(default=0)
	description = models.TextField(blank=True,null=True)
	video_link = EmbedVideoField(blank=True, null=True)  # same like models.URLField()
	completed = models.BooleanField(default=False)
	slug = models.SlugField(
        max_length=200, unique=True, null=True, blank=True)

	class Meta:
		app_label = 'courses'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		self.slug = '-'.join((slugify(self.course.name),
                                           slugify(self.name)))

		super(CourseContent, self).save(*args, **kwargs)

class CourseContentViewedUser(models.Model):
	"""
	Course Content Viewed User
	"""
	user = models.ForeignKey(
		settings.AUTH_USER_MODEL, related_name='user_content_courses', on_delete=models.CASCADE, null=True)
	course_content = models.ForeignKey(
		CourseContent, on_delete=models.CASCADE, related_name='course_contents', null=True)
	course = models.ForeignKey(
		Courses, on_delete=models.CASCADE, related_name='course_contents_views', null=True)
	completed = models.BooleanField(default=False)

	class Meta:
		app_label = 'courses'

	def __str__(self):
		return '{0} - {1}'.format(str(self.user), str(self.course_content))



class CourseReview(models.Model):
    """
    Course Review Model
    """ 
    user = models.ForeignKey(
		settings.AUTH_USER_MODEL, related_name='review_user', on_delete=models.CASCADE, null=True)
    date = models.DateTimeField(auto_now_add=True)
    review = models.TextField()
    rating = models.IntegerField(default=0)
    course = models.ForeignKey(
        Courses, on_delete=models.CASCADE, related_name='course_comment')


    class Meta:
        app_label = 'courses'

    def __str__(self):
        return self.review

class CourseQuestion(models.Model):
	"""
	Course Question
	"""
	user = models.ForeignKey(
		settings.AUTH_USER_MODEL, related_name='question_user', on_delete=models.CASCADE, null=True)
	date = models.DateTimeField(auto_now_add=True)
	question = models.TextField()
	answer = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	course = models.ForeignKey(
		Courses, on_delete=models.CASCADE, related_name='course_question')

	class Meta:
		app_label = 'courses'

	def __str__(self):
		return self.question


class CourseFilesAttached(models.Model):
	"""
	Course Files
	"""
	course = models.ForeignKey(
		Courses, on_delete=models.CASCADE, related_name='course_files')
	file = models.FileField(upload_to="course_files/%Y/%m/%d", validators=[file_size], null=True)

	class Meta:
		app_label = 'courses'

class UserCourses(models.Model):
	"""
	User Courses Model
	"""
	subs_type = (
		('By ADMIN', 'By ADMIN'),
		('Via Razorpay', 'Via Razorpay')
	)

	user = models.ForeignKey(
		settings.AUTH_USER_MODEL, related_name='user_courses', on_delete=models.CASCADE, null=True)
	course = models.ForeignKey(
        Courses, on_delete=models.CASCADE, related_name='subscribed_courses')
	date_of_subscription = models.DateTimeField(auto_now_add=True)
	date_of_expiry = models.DateTimeField(null=True)
	certificate_no = models.CharField(max_length=200, blank=True,null=True)
	total = models.DecimalField(default=0.00, max_digits=20, decimal_places=2)
	completion_mail = models.BooleanField(default=False)
	subscription_type = models.CharField(
		max_length=100, choices=subs_type, default='Via Razorpay', blank=False)
	is_certificate_unlocked = models.BooleanField(default=False)

	def __str__(self):
		return '{0} - {1}'.format(self.user, self.course)

	@cached_property
	def getting_course_completion_percentage(self):
		"""
		Getting course completion percentage
		"""
		course_content = CourseContent.objects.filter(course=self.course)

		course_content_completed = CourseContentViewedUser.objects.filter(user=self.user,course=self.course, completed=True)

		course_content_total = course_content.aggregate(
		    the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Course Content

		course_content_completed_total = course_content_completed.aggregate(
		    the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Course Content Completed

		if course_content:
			percentage_completed = (course_content_completed_total / course_content_total) * 100
		else:
			percentage_completed = 0

		return percentage_completed

	@cached_property
	def getting_quiz_percentage(self):
		"""
		Function to get Quiz Percentage On Completion
		"""
		total_questions = Questions.objects.filter(course=self.course).aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Course Questions

		right_answer_count = UserQuiz.objects.filter(user=self.user, course=self.course, answer__is_correct=True).aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum'] # Total Correct Answer

		if total_questions != 0:
			percentage = (right_answer_count / total_questions) * 100
		else:
			percentage = 0

		return percentage

class Questions(models.Model):
	"""
	Quiz Question Model
	"""
	course = models.ForeignKey(Courses, on_delete=models.CASCADE, related_name='course_questions')
	question = models.TextField()
	order = models.IntegerField(default=0)

	def __str__(self):
		return str(self.question)

	def save(self, *args, **kwargs):
		"""
		Save Function to override order of quiz
		"""
		if not self.order:
			counter = Questions.objects.filter(course=self.course).aggregate(Max('order')).get('order__max')
			if counter:
				counter += 1
			else:
				counter = 1

			self.order = counter

		super(Questions, self).save(*args, **kwargs)

class Answer(models.Model):
	"""
	Answer Model
	"""
	question = models.ForeignKey(Questions, related_name='quiz_answers', on_delete=models.CASCADE)
	answer = models.TextField()
	is_correct = models.BooleanField(default=False)

	def __str__(self):
		return str(self.answer)

class UserQuiz(models.Model):
	"""
	User Quiz Model
	"""
	user = models.ForeignKey(
		settings.AUTH_USER_MODEL, related_name='user_quiz', on_delete=models.CASCADE, null=True)
	course = models.ForeignKey(Courses, on_delete=models.CASCADE, related_name='course_user_questions')
	question = models.ForeignKey(Questions, related_name='user_questions', on_delete=models.CASCADE)
	answer = models.ForeignKey(Answer, on_delete=models.CASCADE, related_name='user_answer')

	def __str__(self):
		return str(self.question.question)


class CourseElements(models.Model):
	"""
	Course Element Changes
	"""
	sample = models.CharField(max_length=100)
	heading = models.TextField()
	text = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	quote = models.TextField(blank=True, null=True)
	image = models.ImageField(
		upload_to='service_image_common', null=True, blank=True, help_text='Image Size 830 x 372')
	about_us_image = models.ImageField(
		upload_to='about_image_service', null=True, blank=True, help_text='Image Size 268 x 99')

	class Meta:
		app_label = 'courses'

	def __str__(self):
		return self.sample

	def save(self, *args, **kwargs):

		if self.image:
			temp_image = Image.open(self.image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((830, 372))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)

		if self.about_us_image:
			temp_image = Image.open(self.about_us_image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((268, 99))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.about_us_image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.about_us_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)

		super(CourseElements, self).save(*args, **kwargs)


class SkillsTrainingVideos(models.Model):
	"""
	Training Videos Model
	"""
	video_link = EmbedVideoField()  # same like models.URLField()

	class Meta:
		app_label = 'courses'