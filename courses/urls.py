"""
Urls
"""
from django.conf.urls import url
from . import views

from django.views.decorators.cache import cache_page

app_name = 'courses'

urlpatterns = [

	url(r'^(?P<course_slug>[\w-]+)/$',
        views.course_details_view, name='course_details'),

    url(r'^category/(?P<category_slug>[\w-]+)/$',
        views.course_category_details_view, name='course_category_details'),

    url(r'^sub-category/(?P<sub_category_slug>[\w-]+)/$',
        views.course_sub_category_details_view, name='course_sub_category_details'),

    url(r'^all-courses/list/$',
        views.all_course_list, name='all_course_list'),

    url(r'^my-courses/list/$',
        views.my_course_list_view, name='my_course_list'),

    url(r'^my-courses/details/(?P<course_slug>[\w-]+)/$',
        views.my_course_details, name='my_course_details'),

    url(r'^courses/details/(?P<course_slug>[\w-]+)/admin/$',
        views.admin_course_details, name='admin_course_details'),

    url(r'^my-courses/content/details/(?P<course_content_pk>\d+)/$',
        views.my_course_content_details, name='my_course_content'),

    url(r'^download-zip-file/(?P<course_pk>\d+)/$',
        views.download_zip_files, name='download_zip_files'),

    url(r'^user-course-list/(?P<profile_pk>\d+)/$',
        views.user_courses_list, name='user_courses_list'),

    url(r'^user/course/(?P<course_pk>\d+)/update/user/(?P<profile_pk>\d+)/$',
        views.course_update_ajax, name='course_update'),

    url(r'^certificate/(?P<course_slug>[\w-]+)/$',
        views.course_completion_certificate, name='course_certificate'),

    url(r'^quiz/courses/$',
        views.course_for_quiz_list_view, name='quiz_course_list'),

    url(r'^(?P<course_slug>[\w-]+)/question/order_no/(?P<question_order>\d+)/$',
        views.create_user_quiz, name='create_user_quiz'),

    url(r'^(?P<course_slug>[\w-]+)/quiz/result/$',
        views.quiz_result_view, name='quiz_result'),

    url(r'^(?P<course_slug>[\w-]+)/quiz/restart/$',
        views.restart_quiz, name='restart_quiz'),


    url(r'^(?P<course_slug>[\w-]+)/quiz/answers/$',
        views.all_users_answer, name='quiz_answers'),

    url(r'^package/list/$',
        cache_page(60*15)(views.PackageListView.as_view()), name='package_list'),

    url(r'^package/details/(?P<package_slug>[\w-]+)/$',
        cache_page(60*15)(views.PackageDetailView.as_view()), name='package_details'),

    url(r'^search/result/$',
        views.search_courses, name='search_courses'),

    url(r'^search/result/admin/$',
        views.search_admin_courses, name='search_admin_courses'),

    url(r'^search/result/student/$',
        views.search_student_courses, name='search_student_courses'),

    url(r'^skills/list/$',
        views.skills_list, name='skills_list'),

    url(r'^add-subscription/courses/(?P<profile_pk>\d+)/$',
        views.user_course_manual_subscription_list, name='user_course_manual_subscription_list'),

    url(r'^add-course/manually/(?P<course_pk>\d+)/user/(?P<profile_pk>\d+)/$',
        views.add_course_to_student, name='add_course_to_student'),

    url(r'^unlock/certificate/(?P<user_courses_pk>\d+)/$',
        views.unlock_certificate, name='unlock_certificate'),

]