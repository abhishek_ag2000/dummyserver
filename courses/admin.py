from django.contrib import admin
from django.contrib.admin import widgets

from .models import CourseCategory, CourseSubCategory, Courses, CoursesPackages, CourseFilesAttached, CourseContent, CourseReview, UserCourses, CourseContentViewedUser, Questions, Answer, UserQuiz, CourseElements, SkillsTrainingVideos
# Register your models here.

class ManyToManyAdmin(admin.ModelAdmin):
    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        kwargs['widget'] = widgets.FilteredSelectMultiple(
            db_field.verbose_name,
            db_field.name in self.filter_vertical
        )

        return super(admin.ModelAdmin, self).formfield_for_manytomany(
            db_field, request=request, **kwargs)

class MyAdmin(admin.ModelAdmin):
    """
    Model Admin Class to disable adding new objects
    """
    readonly_fields = ('sample',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class CourseContentAdmin(admin.TabularInline):
    """
    CourseContent Inline Admin
    """
    model = CourseContent
    fk_name = 'course'


class CourseFilesAttachedAdmin(admin.TabularInline):
    """
    CourseFilesAttached Inline Admin
    """
    model = CourseFilesAttached
    fk_name = 'course'

class CoursesAdmin(admin.ModelAdmin):
    """
    Admin for Courses
    """
    model = Courses
    list_display = ['name', 'duration', 'course_total', ]
    inlines = (CourseContentAdmin,CourseFilesAttachedAdmin,) 

class AnswerAdmin(admin.TabularInline):
    """
    Answer Inline Admin
    """
    model = Answer
    fk_name = 'question'

class QuestionAdmin(admin.ModelAdmin):
    """
    Question Admin
    """
    model = Questions
    list_display = ['question','course']
    inlines = (AnswerAdmin,)
    readonly_fields = ('order',)

class CoursePackageAdmin(ManyToManyAdmin):
    """
    Course Package Admin
    """
    model = CoursesPackages
    list_display = ['package_name','package_price','discount','package_total']


admin.site.register(CourseCategory)
admin.site.register(CourseSubCategory)
admin.site.register(Courses, CoursesAdmin)
admin.site.register(CourseReview)
admin.site.register(UserCourses)
admin.site.register(CourseContentViewedUser)
admin.site.register(Questions,QuestionAdmin)
admin.site.register(UserQuiz)
admin.site.register(CoursesPackages,CoursePackageAdmin)
admin.site.register(CourseElements, MyAdmin)
admin.site.register(SkillsTrainingVideos)