from celery.schedules import crontab
from celery.task import periodic_task
from django.utils import timezone

from .models import Courses, UserCourses


@periodic_task(run_every=crontab(minute='*/5'))
def delete_expired_courses():
	"""
	Celery Task to delete the expired courses
	"""
	# Query all the User Courses in our database
	user_courses = UserCourses.objects.all()

	# Iterate through them
	for item in user_courses:

		 
		if item.date_of_expiry < timezone.now():

			# If the expiration date is bigger than first remove user from course user list
			item.course.subscribed_users.remove(item.user)
			item.course.save()

			# Then Delete the User Course Model
			item.delete()

	return "completed deleting your course at {}".format(timezone.now())




