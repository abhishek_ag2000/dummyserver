from django.shortcuts import render
from django.conf import settings
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives

from .models import WelcomeMail, OrderPlacedMail
# Create your views here.


def send_welcome_mail(user,to_mail):
	"""
	Send Wecome Mail Test
	"""
	welcome_msg = WelcomeMail.objects.filter(sample__exact='WELCOME-MAIL').first()

	subject = welcome_msg.subject

	plaintext = get_template('new_join/welcome_mail.txt')

	htmly     = get_template('new_join/welcome_mail.html')

	context = { 'welcome_msg': welcome_msg, 'user' : user }

	from_email = settings.DEFAULT_FROM_EMAIL

	text_content = plaintext.render(context)
	html_content = htmly.render(context)

	msg = EmailMultiAlternatives(subject, text_content, from_email, [to_mail])
	msg.attach_alternative(html_content, "text/html")
	msg.send()

def order_placed_mail(user,order,cart_total,discount,grand_total,to_mail):
	"""
	Send A Mail To User When Their Order Is Placed
	"""
	placed_msg = OrderPlacedMail.objects.filter(sample__exact='ORDER-PLACED').first()

	subject = placed_msg.subject

	plaintext = get_template('order_placed_mail/order_placed_mail.txt')

	html = get_template('order_placed_mail/order_placed_mail.html')

	context = { 'placed_msg': placed_msg, 'user' : user, 'order' : order, 'cart_total' : cart_total, 'discount' : discount, 'grand_total' : grand_total}

	from_email = settings.DEFAULT_FROM_EMAIL

	text_content = plaintext.render(context)
	html_content = html.render(context)

	msg = EmailMultiAlternatives(subject, text_content, from_email, [to_mail])
	msg.attach_alternative(html_content, "text/html")
	msg.send()

def course_completion_mail(user, course, to_mail):
	"""
	Course Completion Mail
	"""
	subject = "Course Completed"

	plaintext = get_template('course/complete_mail.txt')

	html = get_template('course/complete_mail.html')

	context = { 'user' : user, 'course' : course,}

	from_email = settings.DEFAULT_FROM_EMAIL

	text_content = plaintext.render(context)
	html_content = html.render(context)

	msg = EmailMultiAlternatives(subject, text_content, from_email, [to_mail])
	msg.attach_alternative(html_content, "text/html")
	msg.send()


def send_course_question_mail(contact):
    """
    Send Service Contact Mail
    """
    subject = contact.subject

    plaintext = get_template('course/question.txt')

    htmly     = get_template('course/question.html')

    context = { 'contact' : contact }

    from_email = settings.DEFAULT_FROM_EMAIL

    text_content = plaintext.render(context)
    html_content = htmly.render(context)

    msg = EmailMultiAlternatives(subject, text_content, from_email, ['learning@ankrglobal.com'])
    msg.attach_alternative(html_content, "text/html")
    msg.send()