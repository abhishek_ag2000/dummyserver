from django.apps import AppConfig


class EmailConfigurationsConfig(AppConfig):
    name = 'email_configurations'
