from django.contrib import admin

from .models import WelcomeMail, OrderPlacedMail
# Register your models here.

class MyAdmin(admin.ModelAdmin):
    """
    Model Admin Class to disable adding new objects
    """
    readonly_fields = ('sample',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(WelcomeMail, MyAdmin)
admin.site.register(OrderPlacedMail, MyAdmin)