from django.db import models
from django.utils.text import slugify
from django.conf import settings

from ckeditor_uploader.fields import RichTextUploadingField
# Create your models here.

class WelcomeMail(models.Model):
	"""
	Welcome Mail Model
	"""
	sample = models.CharField(max_length=100, null=True)
	subject = models.CharField(max_length=200)
	text = RichTextUploadingField(
		blank=True, null=True, config_name='special')

	class Meta:
		app_label = 'email_configurations'

	def __str__(self):
		return str(self.sample)

class OrderPlacedMail(models.Model):
	"""
	Order Placed Mail
	"""
	sample = models.CharField(max_length=100)
	subject = models.CharField(max_length=200)
	text = RichTextUploadingField(
		blank=True, null=True, config_name='special')

	def __str__(self):
		return self.sample