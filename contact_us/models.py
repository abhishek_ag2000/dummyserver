from PIL import Image
import sys
import os
from io import BytesIO

from django.db import models
from django.core.validators import RegexValidator
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.files.uploadedfile import InMemoryUploadedFile
# Create your models here.


class Contact_BackGround(models.Model):
    """
    Contact Back Ground Image
    """
    sample = models.CharField(max_length=20)
    contact_title = models.CharField(max_length=100)
    sub_title_text = RichTextUploadingField(
        blank=True, null=True, config_name='special')
    image = models.ImageField(
        upload_to='contact_us/background_image', null=True, blank=True, help_text='Image Size 1920 x 300')

    def __str__(self):
        return self.sample

    def save(self, *args, **kwargs):
        if self.image:
            temp_image = Image.open(self.image).convert('RGB')
            output_io_stream = BytesIO()
            temp_resized_image = temp_image.resize((1920, 350))
            temp_resized_image.save(
                output_io_stream, format='JPEG', quality=60)
            output_io_stream.seek(0)
            self.image = InMemoryUploadedFile(output_io_stream,
                                              'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)

        super(Contact_BackGround, self).save(*args, **kwargs)


class ContactUsQuery(models.Model):
    """
    Query From Contact Us Model
    """
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{6,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    # validators should be a list
    phone_no = models.CharField(
        validators=[phone_regex], max_length=17, null=True)
    subject = models.CharField(max_length=200)
    message = models.TextField()

    class Meta:
        app_label = 'contact_us'

    def __str__(self):
        return self.name
