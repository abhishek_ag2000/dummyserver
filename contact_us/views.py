from django.shortcuts import render
from django.views.generic import CreateView
from django.contrib import messages
from django.conf import settings
from django.urls import reverse
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives

from app.common import CommonMixin
from .models import Contact_BackGround, ContactUsQuery
from .forms import ContactUsQueryForm
# Create your views here.


def send_contact_message_query_mail(contact):
    """
    Send Service Contact Mail
    """
    subject = contact.subject

    plaintext = get_template('query_from_contact/query.txt')

    htmly     = get_template('query_from_contact/query.html')

    context = { 'contact' : contact }

    from_email = settings.DEFAULT_FROM_EMAIL

    text_content = plaintext.render(context)
    html_content = htmly.render(context)

    msg = EmailMultiAlternatives(subject, text_content, from_email, ['learning@ankrglobal.com'])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

class ContactUsQueryCreateView(CommonMixin, CreateView):
    """
    ContactUsQuery Create View
    """
    form_class = ContactUsQueryForm
    template_name = "contact_us.html"

    def get_success_url(self, **kwargs):
        contact_query = ContactUsQuery.objects.filter(id=self.object.id).first()
        # send_contact_message_query_mail(contact=contact_query)
        messages.success(
            self.request, 'Send successfully! Our Team will get in touch with you.')
        return reverse('contact_us:contact_us_form')

    def get_context_data(self, **kwargs):
        context = super(ContactUsQueryCreateView,
                        self).get_context_data(**kwargs)

        context['contact_background'] = Contact_BackGround.objects.filter(
            sample__exact='Contact-Background').first()

        return context
