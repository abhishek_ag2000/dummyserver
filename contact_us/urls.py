"""
Contact (URLs)
"""
from django.conf.urls import url
from . import views

from django.views.decorators.cache import cache_page

app_name = 'contact_us'

urlpatterns = [
    url(r'^$', cache_page(60*15)(views.ContactUsQueryCreateView.as_view()), name='contact_us_form'),
]
