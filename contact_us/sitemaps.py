from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse

class ContactUsSiteMap(Sitemap):
	"""
	Sitemap For Contact Us Url
	"""

	def items(self):
		return ['contact_us:contact_us_form']

	def location(self, item):
		return reverse(item)
