from django.contrib import admin

from .models import Contact_BackGround, ContactUsQuery
# Register your model


class MyAdmin(admin.ModelAdmin):
    """
    Model Admin Class to disable adding new objects
    """
    readonly_fields = ('sample',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Contact_BackGround)
admin.site.register(ContactUsQuery)
