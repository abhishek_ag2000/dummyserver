from django.contrib import admin

from .models import AboutUs, OurSkills
# Register your models here.

class SkillsAdmin(admin.TabularInline):
	"""
	Skills Inline Admin
	"""
	model = OurSkills
	fk_name = 'about'

class AboutAdmin(admin.ModelAdmin):
    """
    About Admin
    """
    readonly_fields = ('sample',)
    inlines = (SkillsAdmin,) 

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(AboutUs, AboutAdmin)
