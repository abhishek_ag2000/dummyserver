"""
About (URLs)
"""
from django.conf.urls import url
from . import views

app_name = 'about_us'

urlpatterns = [
	
	url(r'^$', views.about_us_view, name='about_ankr'),

]