from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse

class AboutUsSiteMap(Sitemap):
	"""
	Sitemap For About Us Url
	"""

	def items(self):
		return ['about_us:about_ankr']

	def location(self, item):
		return reverse(item)