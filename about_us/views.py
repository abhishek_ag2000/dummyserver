from django.shortcuts import render
from django.views.decorators.cache import cache_page


from app.common import CommonMixin
from app.models import Trainers, Landing

from .models import AboutUs, OurSkills
# Create your views here.

@cache_page(60 * 15)
def about_us_view(request):
	"""
	About Us View
	"""
	about = AboutUs.objects.filter(sample__exact='About-Us').first()

	trainers_list = Trainers.objects.all().order_by('id')

	landing = Landing.objects.filter(sample__exact='LANDING').first()

	skills_list = OurSkills.objects.filter(about=about).order_by('-id')[:4]

	context = {
		'about' : about,
		'trainers_list' : trainers_list,
		'landing' : landing,
		'skills_list' : skills_list                   
	}
	return CommonMixin.render(request, 'about_us.html',context)