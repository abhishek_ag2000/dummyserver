from PIL import Image
import sys
import os
from io import BytesIO
from django.db import models
from django.core.validators import URLValidator
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.files.uploadedfile import InMemoryUploadedFile
# Create your models here.


class AboutUs(models.Model):
	"""
	About Us Model
	"""
	sample = models.CharField(max_length=200)
	about_description = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	about_image = models.ImageField(
		upload_to='about_display', null=True, blank=True, help_text='Image Size 500 x 500')
	image = models.ImageField(
		upload_to='about_display', null=True, blank=True, help_text='Image Size 560 x 380')
	background_image = models.ImageField(
		upload_to='about_backgrond', null=True, blank=True, help_text='Image Size 1920 x 350')
	scholar_background = models.ImageField(
		upload_to='scholar_backgrond', null=True, blank=True, help_text='Image Size 1608 x 1020')
	scholar_image = models.ImageField(
		upload_to='about_scholar', null=True, blank=True, help_text='Image Size 423 x 575, PNG Format')
	about_video_link_in_landing = models.TextField(
		validators=[URLValidator()], null=True, blank=True)

	class Meta:
		app_label = 'about_us'

	def __str__(self):
		return self.sample

	def save(self, *args, **kwargs):

		if self.about_image:
			temp_image = Image.open(self.about_image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((500, 500))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.about_image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.about_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)

		if self.image:
			temp_image = Image.open(self.image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((560, 380))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)
        
		if self.background_image:
			temp_image = Image.open(self.background_image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((1920, 350))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.background_image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.background_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)

		if self.scholar_background:
			temp_image = Image.open(self.scholar_background).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((1608, 1020))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.scholar_background = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.scholar_background.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)


		if self.scholar_image:
			temp_image = Image.open(self.scholar_image).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((420, 575))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.scholar_image = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.scholar_image.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)

		super(AboutUs, self).save(*args, **kwargs)

class OurSkills(models.Model):
	about = models.ForeignKey(
        AboutUs, on_delete=models.CASCADE, related_name='about_skills')
	name = models.CharField(max_length=200, blank=True, null=True)
	text = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	button_name = models.CharField(max_length=200, blank=True, null=True)
	button_url = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	image = models.ImageField(
		upload_to='skill_image', null=True, blank=True, help_text='Image Size 200 x 200')

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):

		if self.image:
			temp_image = Image.open(self.image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((200, 200))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)

		super(OurSkills, self).save(*args, **kwargs)