from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView

from app.common import CommonMixin
from .models import FAQ, FaqCategories
# Create your views here.


class FaqListView(CommonMixin, ListView):
	"""
	Faq List View
	"""
	model = FAQ
	context_object_name = 'faq_list'
	template_name = 'faq.html'

	def get_queryset(self):
		return self.model.objects.all().order_by('id')

	def get_context_data(self, **kwargs):
		context = super(FaqListView, self).get_context_data(**kwargs)

		context['faq_categories'] = FaqCategories.objects.all().order_by('id')

		return context
