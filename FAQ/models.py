from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.text import slugify

# Create your models here.

class FaqCategories(models.Model):
	"""
	Faq Category Model
	"""
	name = models.CharField(max_length=150, unique=True)
	slug = models.SlugField(
		max_length=200, unique=True, null=True, blank=True)

	class Meta:
		app_label = 'FAQ'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(FaqCategories, self).save(*args, **kwargs)

class FAQ(models.Model):
	"""
	FAQ Model
	"""
	question = models.TextField()
	answer = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	category = models.ForeignKey(
		FaqCategories, on_delete=models.CASCADE, related_name='faqs', null=True)

	class Meta:
		app_label = 'FAQ'

	def __str__(self):
		return self.question