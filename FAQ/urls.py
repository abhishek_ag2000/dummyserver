"""
Urls
"""
from django.conf.urls import url
from . import views

from django.views.decorators.cache import cache_page

app_name = 'FAQ'

urlpatterns = [
	url(r'^$', cache_page(60*15)(views.FaqListView.as_view()), name='faq_list'),
]