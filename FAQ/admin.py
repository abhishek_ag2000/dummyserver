from django.contrib import admin

from .models import FaqCategories, FAQ

# Register your models here.

admin.site.register(FaqCategories)
admin.site.register(FAQ)