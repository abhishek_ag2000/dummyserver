from django.contrib import admin

from .models import CartItems, DemoItems, Coupon, UserOrders, UserCouponUsage
# Register your models here.

admin.site.register(CartItems)
admin.site.register(DemoItems)
admin.site.register(Coupon)
admin.site.register(UserOrders)
admin.site.register(UserCouponUsage)