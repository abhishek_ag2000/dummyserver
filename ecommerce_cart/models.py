from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.text import slugify

from courses.models import Courses, CoursesPackages
# Create your models here.

class CartItems(models.Model):
	"""
	Cart Items Model
	"""
	user = models.ForeignKey(
		settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='user_cart_items')

	course = models.ForeignKey(
		Courses, on_delete=models.CASCADE, related_name='course_in_cart', blank=True, null=True)

	packages = models.ForeignKey(
		CoursesPackages, on_delete=models.CASCADE, related_name='package_in_cart', blank=True, null=True)

	ref_code = models.CharField(max_length=20,unique=True)
	is_ordered = models.BooleanField(default=False)
	date_ordered = models.DateTimeField(auto_now=True)

	coupon_discount = models.DecimalField(
		default=0.00, max_digits=19, decimal_places=2)

	cart_total = models.DecimalField(
		default=0.00, max_digits=19, decimal_places=2)

	class Meta:
		app_label = 'ecommerce_cart'

	def __str__(self):
		return '{0} - {1}'.format(self.user, self.ref_code)

	def save(self, *args, **kwargs):
		"""
		Save Function For CartItems Model
		"""
		user_applied_coupon = UserAppliedCoupon.objects.filter(user=self.user).first()

		if user_applied_coupon:
			course_coupon_list = user_applied_coupon.coupon.courses.all() # Getting all selected Courses in Coupon
			package_coupon_list = user_applied_coupon.coupon.packages.all() # Getting all selected Packages in Coupon
			if course_coupon_list and self.course: # Checking if any course is selected
				for obj in course_coupon_list:
					if self.course.id == obj.id:
						if user_applied_coupon.coupon.discount_type == 'ON PERCENTAGE':
							discount_course = (obj.course_total * user_applied_coupon.coupon.discount_percentage) / 100
							total_courses = obj.course_total - discount_course
						else:
							discount_course = user_applied_coupon.coupon.discount_amount
							total_courses = obj.course_total - discount_course
						self.coupon_discount = discount_course
						self.cart_total = total_courses
					else:
						if self.course:
							self.coupon_discount = 0
							self.cart_total = self.course.course_total
						elif self.packages:
							self.coupon_discount = 0
							self.cart_total = self.packages.package_total
						else:
							self.cart_total = 0

			elif package_coupon_list and self.packages: # Checking if any packages is selected
				for obj in package_coupon_list:
					if self.packages.id == obj.id:
						if user_applied_coupon.coupon.discount_type == 'ON PERCENTAGE':
							discount_course = (obj.package_total * user_applied_coupon.coupon.discount_percentage) / 100
							total_courses = obj.package_total - discount_course
						else:
							discount_course = user_applied_coupon.coupon.discount_amount
						total_courses = obj.package_total - discount_course

						self.coupon_discount = discount_course
						self.cart_total = total_courses
					else:
						if self.course:
							self.coupon_discount = 0
							self.cart_total = self.course.course_total
						elif self.packages:
							self.coupon_discount = 0
							self.cart_total = self.packages.package_total
						else:
							self.cart_total = 0

			else:
				if self.course:
					self.coupon_discount = 0
					self.cart_total = self.course.course_total
				elif self.packages:
					self.coupon_discount = 0
					self.cart_total = self.packages.package_total
				else:
					self.cart_total = 0
		else:
			if self.course:
				self.coupon_discount = 0
				self.cart_total = self.course.course_total
			elif self.packages:
				self.coupon_discount = 0
				self.cart_total = self.packages.package_total
			else:
				self.cart_total = 0


		super(CartItems, self).save(*args, **kwargs)

class DemoItems(models.Model):
	"""
	Model To Demonstrate if specific product is added to cart or not
	"""
	user = models.ForeignKey(
		settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='user_demo_items')

	course = models.ForeignKey(
		Courses, on_delete=models.CASCADE, related_name='course_in_demo')

	class Meta:
		app_label = 'ecommerce_cart'

	def __str__(self):
		return str(self.course)

class DemoPackageItems(models.Model):
	"""
	Model To Demonstrate if specific product is added to cart or not
	"""
	user = models.ForeignKey(
		settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='user_demo_items_packages')

	packages = models.ForeignKey(
		CoursesPackages, on_delete=models.CASCADE, related_name='package_in_demo')

	class Meta:
		app_label = 'ecommerce_cart'

	def __str__(self):
		return str(self.course)


class Coupon(models.Model):
	"""
	Coupon Model
	"""
	discount_types = (
		('ON SUBTOTAL', 'ON SUBTOTAL'),
		('ON COURSE AND PACKAGES', 'ON COURSE AND PACKAGES')
	)
	d_types = (
		('ON PERCENTAGE', 'ON PERCENTAGE'),
		('ON FIXED PRICE', 'ON FIXED PRICE')
	)
	coupon_type = models.CharField(
		max_length=100, choices=discount_types, default='ON SUBTOTAL', blank=False)
	discount_type = models.CharField(
		max_length=100, choices=d_types, default='ON PERCENTAGE', blank=False)
	coupon_code = models.CharField(max_length=20)
	valid_till = models.DateField(null=True)
	discount_percentage = models.DecimalField(
		default=0.00, max_digits=19, decimal_places=2, blank=True, null=True)
	discount_amount = models.DecimalField(
		default=0.00, max_digits=19, decimal_places=2, blank=True, null=True)
	show_public = models.BooleanField(default=False)
	courses = models.ManyToManyField(
		Courses, related_name='coupon_courses', blank=True)
	packages = models.ManyToManyField(
		CoursesPackages, related_name='coupon_packages', blank=True)

	class Meta:
		app_label = 'ecommerce_cart'

	def __str__(self):
		return self.coupon_code

	def clean(self):
		if self.discount_percentage > 100:
			raise ValidationError({
				'discount_percentage': ["Value cannot be greater than 100"],
			})


class UserAppliedCoupon(models.Model):
	"""
	User Applied Coupon
	"""
	user = models.OneToOneField(
		settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='user_coupon')
	coupon = models.ForeignKey(
		Coupon, related_name='coupons', on_delete=models.CASCADE)

	class Meta:
		app_label = 'ecommerce_cart'

	def __str__(self):
		return '{0} - {1}'.format(self.user, self.coupon.coupon_code)

class UserCouponUsage(models.Model):
	"""
	User Course Usage
	"""
	user = models.ForeignKey(
		settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='user_course_usage')
	date_used = models.DateTimeField(auto_now=True)
	coupon = models.ForeignKey(
		Coupon, related_name='coupons_used', on_delete=models.CASCADE)

	def __str__(self):
		return '{0} - {1}'.format(self.user, self.coupon.coupon_code)

class UserOrders(models.Model):
	"""
	User Order Model
	"""
	user = models.ForeignKey(
		settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='user_orders')
	date_ordered = models.DateTimeField(auto_now=True)
	ref_code = models.CharField(max_length=20, unique=True)
	coupon_applied = models.ForeignKey(
		Coupon, on_delete=models.CASCADE, related_name='user_order_coupons', blank=True, null=True)
	courses = models.ManyToManyField(
		CartItems, related_name='user_order_courses', blank=True)
	sub_total = models.DecimalField(
		default=0.00, max_digits=19, decimal_places=2, null=True)
	promo_per = models.DecimalField(
		default=0.00, max_digits=19, decimal_places=2, null=True)
	promo_amount = models.DecimalField(
		default=0.00, max_digits=19, decimal_places=2, null=True)
	grand_total = models.DecimalField(
		default=0.00, max_digits=19, decimal_places=2, null=True)
	slug = models.SlugField(
        max_length=200, unique=True, null=True, blank=True)
	razorpay_order_id = models.CharField(max_length=50, null=True, blank=True)
	razorpay_order_amount = models.PositiveIntegerField(default=0)  # in paise
	razorpay_payment_id = models.CharField(max_length=50, null=True, blank=True)
	razorpay_signature = models.CharField(max_length=255, null=True, blank=True)
	razorpay_order_status = models.CharField(max_length=20, null=True, blank=True)
	razorpay_attempts = models.PositiveIntegerField(default=0)

	def __str__(self):
		return '{0} - {1}'.format(self.user, self.ref_code)

	def save(self, *args, **kwargs):
		"""
		Save Function To create a slug object
		"""
		self.slug = '-'.join((slugify(self.user),
                                           slugify(self.ref_code)))
		super(UserOrders, self).save(*args, **kwargs)


