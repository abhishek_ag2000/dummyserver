import decimal
import datetime
from datetime import date
from dateutil.relativedelta import relativedelta

from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib import messages
from django.db.models import Q
from django.views.generic import ListView, CreateView, UpdateView
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum, Value, Count
from django.db.models.functions import Coalesce
from django.http import HttpResponseRedirect
from django.db import transaction

from app.common import CommonMixin, CommonAdminMixin

from .razorpay import RAZORPAY_KEY, new_razorpay_order, fetch_razorpay_order_info, verify_razorpay_signature
from .etran_validation import subscribe_course

from courses.models import Courses, UserCourses, CoursesPackages
from .extras import generate_order_id
from .forms import CouponForm
from .models import CartItems, DemoItems, DemoPackageItems, Coupon, UserAppliedCoupon, UserOrders, UserCouponUsage
# Create your views here.

@login_required
def cart_item_list(request):
    """
    Cart View
    """
    existing_orders = CartItems.objects.filter(
        user=request.user, is_ordered=False)  # Getting Items in Cart

    user_applied_coupon = UserAppliedCoupon.objects.filter(user=request.user).first()


    cart_total = existing_orders.aggregate(
        the_sum=Coalesce(Sum('cart_total'), Value(0)))['the_sum']  # Cart Sub Total Calculation

    grand_total = cart_total

    discount = 0

    if user_applied_coupon: 
        if user_applied_coupon.coupon.coupon_type == 'ON SUBTOTAL': # For Altogether Discount on Final Cart Amount
            if user_applied_coupon.coupon.discount_type == 'ON PERCENTAGE':
                discount = (cart_total * user_applied_coupon.coupon.discount_percentage) / 100
                grand_total = cart_total - discount
            else:
                discount = user_applied_coupon.coupon.discount_amount
                grand_total = cart_total - user_applied_coupon.coupon.discount_amount



    query = request.GET.get('q')

    result = None

    if query:
        result = Coupon.objects.filter(coupon_code__exact=query)
        if not result:
            messages.error(request, 'Discount Coupon Is Invalid')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
    elif query == "":
        result = None
        messages.error(request, 'Coupon code is Blank')
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
    else:
        result = None

    if result:
        for r in result:
            usage_count = UserCouponUsage.objects.filter(user=request.user, coupon=r).first()
            if datetime.datetime.now().date() > r.valid_till:
                messages.error(request, 'Discount Coupon No Longer Exist')
            elif usage_count:
                messages.error(request, 'Discount Coupon Already Used By You on ' + str(usage_count.date_used.date()))
            else:
                user_coupon = UserAppliedCoupon.objects.create(
                        user=request.user, coupon=r)
                user_coupon.save() 
                coupon_usage = UserCouponUsage.objects.create(
                        user=request.user, coupon=r)
                coupon_usage.save()
                for item in existing_orders:
                    item.save()
                messages.success(
                        request, 'Discount Coupon Applied Successfully')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))


    context = {
    	'existing_orders' : existing_orders,
    	'cart_total' : cart_total,
        'coupon_discount': round(discount, 2),
        'grand_total' : round(grand_total,2),
        'user_applied_coupon' : user_applied_coupon 
    }
    return CommonMixin.render(request, 'cart.html', context)


@login_required
def add_to_cart(request, **kwargs):
    """
    Add To Cart View
    """

    existing_orders = CartItems.objects.filter(
        user=request.user, is_ordered=False)

    course = Courses.objects.filter(id=kwargs.get('course_id', "")).first()

    if existing_orders:
        for item in existing_orders:
            if item.packages:
                for obj in item.packages.courses.all():
                    if obj.id == course.id:
                        messages.error(request, 'This course is already under '+ item.packages.package_name +' package added to cart earlier')
                        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

    add_item = CartItems.objects.create(
        course=course, user=request.user, ref_code=generate_order_id(), is_ordered=False)

    add_item.save()

    add_demo = DemoItems.objects.create(user = request.user, course=course)

    add_demo.save()

    messages.success(
        request, 'Selected Product is successfully added to cart')

    return HttpResponseRedirect(request.META.get("HTTP_REFERER"))


@login_required
def delete_from_cart(request, item_id):
    """
    Delete One Item From Cart
    """

    item_to_delete = CartItems.objects.filter(pk=item_id).first()


    if item_to_delete.course:
        delete_demo = DemoItems.objects.filter(user = request.user, course=item_to_delete.course).first()

        delete_demo.delete()

    if item_to_delete.packages:
        delete_demo_package = DemoPackageItems.objects.filter(user = request.user, packages=item_to_delete.packages).first()
        delete_demo_package.delete()
    
    if item_to_delete:
        item_to_delete.delete()

    messages.success(
        request, 'Selected Product is successfully removed from cart')

    return redirect(reverse('ecommerce_cart:cart_view'))


@login_required
def add_package_to_cart(request, **kwargs):
    """
    Add To Cart View
    """
    existing_orders = CartItems.objects.filter(
        user=request.user, is_ordered=False)

    package = CoursesPackages.objects.filter(id=kwargs.get('package_id', "")).first()

    flag = 0
    if existing_orders:
        for item in existing_orders:
            for obj in package.courses.all():
                if obj.id == item.course.id: 
                    demo_items = DemoItems.objects.filter(user=request.user, course=obj)
                    for dem in demo_items:
                        dem.delete()
                    item.delete()
                    flag = 1
    

    add_item = CartItems.objects.create(
        packages=package, user=request.user, ref_code=generate_order_id(), is_ordered=False)

    add_item.save()

    add_demo = DemoPackageItems.objects.create(user=request.user, packages=package)

    add_demo.save()

    if flag == 1:
        messages.success(request, 'Looks Like one of the courses in your cart is already under this package we have adjusted it for you')
    else:
        messages.success(request, 'Selected Package is successfully added to cart')

    return HttpResponseRedirect(request.META.get("HTTP_REFERER"))



@login_required
def clear_cart(request):
    """
    Clear All Item From Cart View
    """
    item_to_delete = CartItems.objects.filter(
        user=request.user, is_ordered=False)

    for item in item_to_delete:
        item.delete()
    messages.success(
        request, 'Cart is cleared successfully')
    return redirect(reverse('ecommerce_cart:cart_view'))

@login_required
def remove_coupon(request, coupon_pk):
    """
    Remove Coupon View
    """
    coupon_details = Coupon.objects.filter(pk=coupon_pk).first()

    user_coupon = UserAppliedCoupon.objects.filter(
        user=request.user).first()

    user_coupon.delete()

    coupon_usage = UserCouponUsage.objects.filter(
                        user=request.user, coupon=coupon_details).first()

    coupon_usage.delete()

    existing_orders = CartItems.objects.filter(
        user=request.user, is_ordered=False)  # Getting Items in Cart

    for item in existing_orders:
        item.save()

    messages.success(
        request, 'Discount Coupon removed successfully')
    return redirect(reverse('ecommerce_cart:cart_view'))

@login_required
def check_out(request):
    """
    Check Out View
    """
    existing_orders = CartItems.objects.filter(
        user=request.user, is_ordered=False)  # Getting Items in Cart

    user_applied_coupon = UserAppliedCoupon.objects.filter(user=request.user).first()


    cart_total = existing_orders.aggregate(
        the_sum=Coalesce(Sum('cart_total'), Value(0)))['the_sum']  # Cart Sub Total Calculation

    grand_total = cart_total

    razorpay_order_amount = int(round(grand_total*decimal.Decimal(100.00), 0))

    discount = 0

    if user_applied_coupon: 
        if user_applied_coupon.coupon.coupon_type == 'ON SUBTOTAL': # For Altogether Discount on Final Cart Amount
            if user_applied_coupon.coupon.discount_type == 'ON PERCENTAGE':
                discount = (cart_total * user_applied_coupon.coupon.discount_percentage) / 100
                grand_total = cart_total - discount
                razorpay_order_amount = int(round(grand_total*decimal.Decimal(100.00), 0))
            else:
                discount = user_applied_coupon.coupon.discount_amount
                grand_total = cart_total - user_applied_coupon.coupon.discount_amount
                razorpay_order_amount = int(round(grand_total*decimal.Decimal(100.00), 0))



    if grand_total > 0:
        try:
            razorpay_order_info = new_razorpay_order(razorpay_order_amount, generate_order_id(), {})
            razorpay_order_id = razorpay_order_info.get('id')
        except Exception as ex:
            raise Http404("The server is experiencing issue with the payment gateway. Please try again later.")
    else:
        razorpay_order_id = None


    context = {
        'existing_orders' : existing_orders,
        'user_applied_coupon' : user_applied_coupon,
        'cart_total' : cart_total,
        'discount' : round(discount , 2),
        'grand_total' : round(grand_total , 2),
        'razorpay_order_amount' : razorpay_order_amount,
        'razorpay_order_id' : razorpay_order_id,
        'RAZORPAY_KEY': RAZORPAY_KEY,

    }
    return CommonMixin.render(request, 'check_out.html', context)

@login_required
def place_order_view(request):
    """
    View to receive payment details by the gateway
    """
    razorpay_order_id = request.POST.get('razorpay_order_id', None)  # Value Like: order_DE80c2ju4IhV2p
    razorpay_payment_id = request.POST.get('razorpay_payment_id', None)  # Value Like: pay_DE80lg9yYnkmRW
    razorpay_signature = request.POST.get('razorpay_signature', None)  # Value Like: 180b97f56771a5ad0b84797d0febe957d6ec06b6bb18bea95470a55f3d37d0db

    # for key, value in request.POST.items():
    #     print(key, value)



    if not razorpay_order_id or not razorpay_payment_id or not razorpay_signature:
        messages.error(request, "Payment failed!")
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

    try:
        verify_razorpay_signature(razorpay_order_id, razorpay_payment_id, razorpay_signature)

        razorpay_order_info = fetch_razorpay_order_info(razorpay_order_id)
    #captured_data = capture_razorpay_payment(razorpay_payment_id, order.razorpay_order_amount)

        with transaction.atomic():
            razorpay_order_status = razorpay_order_info.get('status')
            razorpay_attempts = razorpay_order_info.get('attempts')
            razorpay_payment_id = razorpay_payment_id
            razorpay_signature = razorpay_signature

            existing_orders = CartItems.objects.filter(
                user=request.user, is_ordered=False)  # Getting Items in Cart

            user_applied_coupon = UserAppliedCoupon.objects.filter(user=request.user).first()


            cart_total = existing_orders.aggregate(
                the_sum=Coalesce(Sum('cart_total'), Value(0)))['the_sum']  # Cart Sub Total Calculation

            discount = 0

            if user_applied_coupon:
                if user_applied_coupon.coupon.coupon_type == 'ON SUBTOTAL': # For Altogether Discount on Final Cart Amount
                    if user_applied_coupon.coupon.discount_type == 'ON PERCENTAGE':
                        discount = (cart_total * user_applied_coupon.coupon.discount_percentage) / 100
                        grand_total = cart_total - discount
                        grand_total_in_paisa = int(round(grand_total*decimal.Decimal(100.00), 0))  # convert bill rupee to paise
                        orders = UserOrders.objects.create(user=request.user, ref_code=generate_order_id(),coupon_applied=user_applied_coupon.coupon,
                                                            sub_total=cart_total,promo_per=0,promo_amount=user_applied_coupon.coupon.discount_value,
                                                            grand_total=grand_total, razorpay_order_id=razorpay_order_id, razorpay_order_amount=grand_total_in_paisa,
                                                            razorpay_payment_id=razorpay_payment_id, razorpay_signature=razorpay_signature, razorpay_order_status=razorpay_order_status,
                                                            razorpay_attempts=razorpay_attempts)
                    else:
                        discount = user_applied_coupon.coupon.discount_amount
                        grand_total = cart_total - user_applied_coupon.coupon.discount_amount
                        grand_total_in_paisa = int(round(grand_total*decimal.Decimal(100.00), 0))
                        orders = UserOrders.objects.create(user=request.user, ref_code=generate_order_id(),coupon_applied=user_applied_coupon.coupon,
                                                            sub_total=cart_total,promo_per=0,promo_amount=user_applied_coupon.coupon.discount_value,
                                                            grand_total=grand_total, razorpay_order_id=razorpay_order_id, razorpay_order_amount=grand_total_in_paisa,
                                                            razorpay_payment_id=razorpay_payment_id, razorpay_signature=razorpay_signature, razorpay_order_status=razorpay_order_status,
                                                            razorpay_attempts=razorpay_attempts)
            else:
                grand_total = cart_total
                grand_total_in_paisa = int(round(grand_total*decimal.Decimal(100.00), 0))  # convert bill rupee to paise
                orders = UserOrders.objects.create(user=request.user, ref_code=generate_order_id(), sub_total=cart_total,grand_total=grand_total, razorpay_order_id=razorpay_order_id, razorpay_order_amount=grand_total_in_paisa,
                                                    razorpay_payment_id=razorpay_payment_id, razorpay_signature=razorpay_signature, razorpay_order_status=razorpay_order_status,
                                                    razorpay_attempts=razorpay_attempts)


            orders.save()

            user_orders = UserOrders.objects.filter(pk=orders.id).first()

            subscribe_course(request.user, user_orders)


        messages.success(request, 'Your Order is placed successfully')

    except Exception as ex:
        print(ex)
        messages.error(request, 'The server is experiencing issue with the payment gateway. Your payment may have been done. Please contact support.')
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

    return redirect(reverse('ecommerce_cart:thanks', kwargs={"user_order_slug": user_orders.slug}))


@login_required
def finish_submit_view(request):
    """
    Finish Payment with zero order value
    """
    existing_orders = CartItems.objects.filter(
        user=request.user, is_ordered=False)  # Getting Items in Cart

    user_applied_coupon = UserAppliedCoupon.objects.filter(user=request.user).first()


    cart_total = existing_orders.aggregate(
        the_sum=Coalesce(Sum('cart_total'), Value(0)))['the_sum']  # Cart Sub Total Calculation


    if user_applied_coupon:
        grand_total = cart_total - user_applied_coupon.coupon.discount_value
        orders = UserOrders.objects.create(user=request.user, ref_code=generate_order_id(),coupon_applied=user_applied_coupon.coupon,
                                            sub_total=cart_total,promo_per=0,promo_amount=user_applied_coupon.coupon.discount_value,
                                            grand_total=grand_total)
    else:
        grand_total = cart_total
        orders = UserOrders.objects.create(user=request.user, ref_code=generate_order_id(), sub_total=cart_total,grand_total=grand_total)

    orders.save()

    user_orders = UserOrders.objects.filter(pk=orders.id).first()

    for product in existing_orders:
        user_orders.courses.add(product)
        user_orders.save()

        if product.course:
            expiry_month = product.course.course_valid_till_months
            date_of_expiry = date.today() + relativedelta(months=+expiry_month)
            user_courses = UserCourses.objects.create(user=request.user,course=product.course, date_of_expiry=date_of_expiry, total=product.course.course_total)
            user_courses.save()

            product.course.subscribed_users.add(request.user)
            product.course.save()

        if product.packages:
            for item in product.packages.courses.all():
                user_prev_order = UserCourses.objects.filter(user=request.user, course=item).first()
                expiry_month_pac = item.course_valid_till_months
                date_of_expiry_pac = date.today() + relativedelta(months=+expiry_month_pac)
                if user_prev_order:
                    user_prev_order.date_of_expiry = date_of_expiry_pac
                    user_prev_order.save()
                else:
                    user_courses_pac = UserCourses.objects.create(user=request.user,course=item, date_of_expiry=date_of_expiry_pac, total=item.course_total)
                    user_courses_pac.save()
                    item.subscribed_users.add(request.user)
                    item.save()

            product.packages.subscribed_users.add(request.user)
            product.packages.save()

        product.is_ordered = True
        product.save()

    if user_applied_coupon:
        user_applied_coupon.delete()

    demo_items = DemoItems.objects.filter(user=request.user)

    for obj in demo_items:
        obj.delete()

    demo_package_items = DemoPackageItems.objects.filter(user=request.user)

    for j in demo_package_items:
        j.delete()


    messages.success(
        request, 'Your Order is placed successfully')

    return redirect(reverse('ecommerce_cart:thanks', kwargs={"user_order_slug": orders.slug}))

@login_required
def thanks_page(request, user_order_slug):
    """ 
    Order Placed View
    """
    orders = UserOrders.objects.filter(slug=user_order_slug).first()

    if orders.user != request.user:
        messages.error(request, 'You have no permission to access the requested resource!')
        return redirect(reverse('index'))

    context = {
        'orders' : orders
    }
    return CommonMixin.render(request, 'user_order.html', context)

def user_order_list(request):
    """
    User Order List
    """
    order_list = UserOrders.objects.filter(user=request.user).order_by('id')

    context = {
        'order_list' : order_list
    }
    return CommonMixin.render(request, 'order_list.html', context)

def order_details(request, user_order_slug):
    """
    Order Details View
    """
    orders = UserOrders.objects.filter(slug=user_order_slug).first()

    if orders.user != request.user:
        messages.error(request, 'You have no permission to access the requested resource!')
        return redirect(reverse('index'))

    context = {
        'orders' : orders
    }
    return CommonMixin.render(request, 'order_details.html', context)


class CouponListView(LoginRequiredMixin, CommonAdminMixin, ListView):
    """
    Coupon List View
    """
    model = Coupon
    context_object_name = 'coupon_list'
    template_name = 'coupon_list.html'
    paginate_by = 15

    def get(self, request, *args, **kwargs):
        # checking if user is user user
        if not request.user.is_superuser:
            messages.error(request, 'You have no permission to access the requested resource!')
            return redirect(reverse('index_student'))

        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        coupon_list = Coupon.objects.all().order_by('-id')
        return coupon_list

class CouponCreateView(LoginRequiredMixin, CommonAdminMixin, CreateView):
    """
    Coupon Create View
    """
    form_class = CouponForm
    template_name = 'coupon_form.html'

    def get(self, request, *args, **kwargs):
        # checking if user is user user
        if not request.user.is_superuser:
            messages.error(request, 'You have no permission to access the requested resource!')
            return redirect(reverse('index_student'))

        return super().get(request, *args, **kwargs)

    def get_success_url(self, **kwargs):
        return reverse('ecommerce_cart:coupon_list')

class CouponUpdateView(LoginRequiredMixin, CommonAdminMixin, UpdateView):
    """
    Coupon Update View
    """
    model = Coupon
    form_class = CouponForm
    template_name = 'coupon_form.html'

    def get(self, request, *args, **kwargs):
        # checking if user is user user
        if not request.user.is_superuser:
            messages.error(request, 'You have no permission to access the requested resource!')
            return redirect(reverse('index_student'))

        return super().get(request, *args, **kwargs)

    def get_success_url(self, **kwargs):
        messages.success(self.request, 'Your Coupon is Updated Successfully')
        return reverse('ecommerce_cart:coupon_list')

    def get_object(self):
        coupon_details = Coupon.objects.filter(pk=self.kwargs['coupon_pk']).first()
        return coupon_details

@login_required
def delete_coupon(request, coupon_pk):
    """
    Delete Coupon View
    """
    coupon_details = Coupon.objects.filter(
        pk=coupon_pk).first()

    coupon_details.delete()

    messages.success(
        request, 'Coupon deleted successfully')
    return redirect(reverse('ecommerce_cart:coupon_list'))


def coupon_search_autocomplete_ajax(request):
    """
    Ajax Request View For Search Coupon Autocomplete
    """

    query = request.GET.get('search', None)

    if query:
        result = Coupon.objects.filter(
            Q(coupon_code__icontains=query) |
            Q(discount_type__icontains=query) |
            Q(coupon_type__icontains=query) |
            Q(discount_percentage__icontains=query) |
            Q(discount_amount__icontains=query) 
            ).order_by("-id")[:15]
    else:
        result = Coupon.objects.all().order_by('-id')


    context = {
        'coupon_list': result,  # user profile by page
        'last_query': query,
    }

    if request.is_ajax():
        html = render_to_string('coupon_ajax_list.html',
                            context, request=request)

    data = {

        'html' : html,
        'query' : query,
    }

    return JsonResponse(data)

@login_required
def coupon_details_view(request, coupon_pk):
    """
    Coupon Details
    """
    if not request.user.is_superuser:
        messages.error(request, 'You have no permission to access the requested resource!')
        return redirect(reverse('index_student'))

    coupon_details = Coupon.objects.filter(pk=coupon_pk).first()

    user_adjusted_coupon = UserCouponUsage.objects.filter(coupon=coupon_details)

    total_used = user_adjusted_coupon.aggregate(
        the_sum=Coalesce(Count('id'), Value(0)))['the_sum']

    user_coupon_used_list = UserOrders.objects.filter(coupon_applied=coupon_details).order_by('-id')

    context = {
        'coupon_details' : coupon_details,
        'user_adjusted_coupon' : user_adjusted_coupon,
        'total_used' : total_used,
        'user_coupon_used_list' : user_coupon_used_list
    }
    return CommonAdminMixin.render(request, 'coupon_details.html', context)

@login_required
def item_in_cart_while_coupons(request, order_pk):
    """
    Items per user when coupon applied
    """
    if not request.user.is_superuser:
        messages.error(request, 'You have no permission to access the requested resource!')
        return redirect(reverse('index_student'))


    order_details = UserOrders.objects.filter(pk=order_pk).first()

    cart_items = order_details.courses.all()

    context = {
        'order_details' : order_details,
        'cart_items' : cart_items
    }
    return CommonAdminMixin.render(request, 'coupon_order_details.html', context)