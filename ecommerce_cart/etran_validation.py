"""
E-Commerce Transaction Validation
"""

import decimal
import math
import datetime
from datetime import date
from dateutil.relativedelta import relativedelta
from django.db import transaction
from django.http import Http404

from .razorpay import fetch_razorpay_order_info, fetch_razorpay_payment_by_order


from courses.models import Courses, UserCourses, CoursesPackages
from email_configurations.views import order_placed_mail

from .extras import generate_order_id
from .models import CartItems, DemoItems, DemoPackageItems, Coupon, UserAppliedCoupon, UserOrders



def subscribe_course(user, user_orders):
	"""
	Subscribe Course after payment
	"""
	existing_orders = CartItems.objects.filter(
	user=user, is_ordered=False)  # Getting Items in Cart

	user_applied_coupon = UserAppliedCoupon.objects.filter(user=user).first()



	for product in existing_orders:
		user_orders.courses.add(product)
		user_orders.save()

	if product.course:
	    expiry_month = product.course.course_valid_till_months
	    date_of_expiry = date.today() + relativedelta(months=+expiry_month)
	    user_courses = UserCourses.objects.create(user=user,course=product.course, date_of_expiry=date_of_expiry)
	    user_courses.save()

	    product.course.subscribed_users.add(user)
	    product.course.save()

	if product.packages:
	    for item in product.packages.courses.all():
	        user_prev_order = UserCourses.objects.filter(user=user, course=item).first()
	        expiry_month_pac = item.course_valid_till_months
	        date_of_expiry_pac = date.today() + relativedelta(months=+expiry_month_pac)
	        if user_prev_order:
	            user_prev_order.date_of_expiry = date_of_expiry_pac
	            user_prev_order.save()
	        else:
	            user_courses_pac = UserCourses.objects.create(user=user,course=item, date_of_expiry=date_of_expiry_pac)
	            user_courses_pac.save()
	            item.subscribed_users.add(user)
	            item.save()

	    product.packages.subscribed_users.add(user)
	    product.packages.save()

	product.is_ordered = True
	product.save()

	if user_applied_coupon:
		user_applied_coupon.delete()

	demo_items = DemoItems.objects.filter(user=user)

	for obj in demo_items:
		obj.delete()

	demo_package_items = DemoPackageItems.objects.filter(user=user)

	for j in demo_package_items:
		j.delete()

	order_placed_mail(user=user,order=user_orders,cart_total=user_orders.sub_total,discount=user_orders.promo_amount,grand_total=user_orders.grand_total,to_mail=user.profile.email)