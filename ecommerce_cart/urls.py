"""
Urls
"""
from django.conf.urls import url
from . import views

app_name = 'ecommerce_cart'

urlpatterns = [
	url(r'^$', views.cart_item_list, name='cart_view'),
	
	url(r'^add-to-cart/(?P<course_id>[-\w]+)/$',
        views.add_to_cart, name="add_to_cart"),

	url(r'^add-package-to-cart/(?P<package_id>[-\w]+)/$',
        views.add_package_to_cart, name="add_package_to_cart"),

	url(r'^item/delete/(?P<item_id>[-\w]+)/$',
        views.delete_from_cart, name='delete_item'),

	url(r'^items/clear/$',
		views.clear_cart, name='clear_items'),

	url(r'^coupon/(?P<coupon_pk>\d+)/clear/$',
        views.remove_coupon, name='clear_coupon'),

	url(r'^checkout/$',
        views.check_out, name='check_out'),

	url(r'^create/order/$',
        views.place_order_view, name='place_order'),

	url(r'^create/free/order/payment/$',
        views.finish_submit_view, name='finish_submit_view'),

	url(r'^order/confirmed/(?P<user_order_slug>[-\w]+)/$',
        views.thanks_page, name='thanks'),

	url(r'^your/order/$',
        views.user_order_list, name='user_order_list'),

	url(r'^order/details/(?P<user_order_slug>[-\w]+)/$',
        views.order_details, name='order_details'),

	url(r'^coupon/list/$',
        views.CouponListView.as_view(), name='coupon_list'),

	url(r'^coupon/search/$',
        views.coupon_search_autocomplete_ajax, name='coupon_search'),

	url(r'^coupon/create/$',
        views.CouponCreateView.as_view(), name='coupon_create'),

	url(r'^coupon/(?P<coupon_pk>\d+)/update/$',
        views.CouponUpdateView.as_view(), name='coupon_update'),

	url(r'^coupon/(?P<coupon_pk>\d+)/delete/$',
        views.delete_coupon, name='coupon_delete'),

	url(r'^coupon/(?P<coupon_pk>\d+)/details/$',
        views.coupon_details_view, name='coupon_details'),

	url(r'^coupon/order/(?P<order_pk>\d+)/details/$',
        views.item_in_cart_while_coupons, name='item_in_cart_while_coupons'),

]