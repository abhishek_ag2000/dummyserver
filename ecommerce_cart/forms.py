"""
Forms
"""
from django import forms
from django.conf import settings

from courses.models import Courses, CoursesPackages

from .models import Coupon

from django.contrib.admin.widgets import FilteredSelectMultiple



class DateInput(forms.DateInput):
    """
    Widgets support for date input
    """
    input_type = 'date'


class CouponForm(forms.ModelForm):
	"""
	Coupon Form
	"""

	def __init__(self, *args, **kwargs):
		super(CouponForm, self).__init__(*args, **kwargs)

		for field in self.fields.values():
			field.widget.attrs = {"class": "form-control"}

		self.fields['coupon_type'].widget.attrs = {
			'class': 'select2_demo_1 form-control','onchange': 'coupon_type_change(this)', }

		self.fields['discount_type'].widget.attrs = {
			'class': 'select2_demo_4 form-control','onchange': 'discount_type_change(this)', }

		self.fields['courses'].widget.attrs = {
			'class': 'select2_demo_3 form-control','multiple' : 'multiple', }

		self.fields['packages'].widget.attrs = {
			'class': 'select2_demo_2 form-control','multiple' : 'multiple', }


	class Meta:
		model = Coupon
		widgets = {
			'valid_till': DateInput(),
		}
		fields = ['coupon_code', 'coupon_type','discount_type','valid_till','discount_percentage','discount_amount','courses','packages',]