"""
Technology (URLs)
"""
from django.conf.urls import url
from . import views

from django.views.decorators.cache import cache_page

app_name = 'technology'

urlpatterns = [
	
	url(r'^$', cache_page(60*15)(views.TechnologyListView.as_view()), name='technology_list'),

	url(r'^(?P<technology_slug>[\w-]+)/details/$',
        cache_page(60*15)(views.TechnologyDetailView.as_view()), name='technology-details'),

	url(r'^enquiry/(?P<technology_slug>[\w-]+)/$',
        cache_page(60*15)(views.TechnologyQueryCreateView.as_view()), name='technical_enquiry'),

]