"""
Forms
"""
from django import forms

from .models import TechnologyEnquiry


class TechnologyEnquiryForm(forms.ModelForm):
    """
    TechnologyEnquiry Form
    """
    class Meta:
        model = TechnologyEnquiry
        fields = ['name', 'email', 'phone_no', 'subject', 'message', ]

    def __init__(self, *args, **kwargs):
        super(TechnologyEnquiryForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs = {
            'name': 'name', 'placeholder': 'Full Name*', }
        self.fields['email'].widget.attrs = {
            'name': 'email', 'placeholder': 'Enter email*', }
        self.fields['phone_no'].widget.attrs = {
            'name': 'phone', 'placeholder': 'Phone Number*', }
        self.fields['subject'].widget.attrs = {
            'name': 'subject', 'placeholder': 'Subject*', }
        self.fields['message'].widget.attrs = {
            'name': 'message', 'placeholder': 'Write message*', }
