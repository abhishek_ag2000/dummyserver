from django.shortcuts import render
from django.contrib import messages
from django.conf import settings
from django.urls import reverse
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.views.generic import ListView, DetailView, CreateView

from about_us.models import AboutUs
from app.common import CommonMixin
from app.models import ConsultancyAndTechnologyFeedback
from contact_us.models import Contact_BackGround

from .forms import TechnologyEnquiryForm 
from .models import Technology, TechnologyElements, TechnologyTabs, TechnologyEnquiry
# Create your views here.


def send_tech_query_message_query_mail(contact):
    """
    Send Service Contact Mail
    """
    subject = contact.subject

    plaintext = get_template('query_from_tech/query.txt')

    htmly     = get_template('query_from_tech/query.html')

    context = { 'contact' : contact }

    from_email = settings.DEFAULT_FROM_EMAIL

    text_content = plaintext.render(context)
    html_content = htmly.render(context)

    msg = EmailMultiAlternatives(subject, text_content, from_email, ['learning@ankrglobal.com'])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


class TechnologyListView(CommonMixin, ListView):
	"""
	Technology List View
	"""
	model = Technology
	context_object_name = 'technology_list'
	template_name = 'technology_list.html'
	paginate_by = 12

	def get_queryset(self):
		return self.model.objects.all().order_by('-id')

	def get_context_data(self, **kwargs):
		context = super(TechnologyListView, self).get_context_data(**kwargs)

		context['review_list'] = ConsultancyAndTechnologyFeedback.objects.all().order_by('id')

		context['technology_elements'] = TechnologyElements.objects.filter(sample__exact='TECHNOLOGY-ELEMENTS').first()

		context['about'] = AboutUs.objects.filter(sample__exact='About-Us').first()

		return context


class TechnologyDetailView(CommonMixin, DetailView):
	"""
	Technology Details View
	"""
	context_object_name = 'technology_details'
	model = Technology
	template_name = 'technology_details.html'

	def get_object(self):
		technology_details = Technology.objects.filter(slug=self.kwargs['technology_slug']).first()
		return technology_details

	def get_context_data(self, **kwargs):
		context = super(TechnologyDetailView, self).get_context_data(**kwargs)

		context['technology_list'] = Technology.objects.all().order_by('id')

		context['tab_list'] = TechnologyTabs.objects.filter(technology=self.get_object()).order_by('id')

		return context


class TechnologyQueryCreateView(CommonMixin, CreateView):
	"""
	ContactUsQuery Create View
	"""
	form_class = TechnologyEnquiryForm
	template_name = "technology_enquiry.html"

	def get_success_url(self, **kwargs):
		technology_details = Technology.objects.filter(slug=self.kwargs['technology_slug']).first()
		technical_enquiry = TechnologyEnquiry.objects.filter(id=self.object.id).first()
		# send_tech_query_message_query_mail(contact=technical_enquiry)
		messages.success(
	    	self.request, 'Send successfully! Our Team will get in touch with you.')
		return reverse('technology:technical_enquiry', kwargs={'technology_slug': technology_details.slug})

	def form_valid(self, form):
		technology_details = Technology.objects.filter(slug=self.kwargs['technology_slug']).first()
		form.instance.technology = technology_details
		return super(TechnologyQueryCreateView, self).form_valid(form)

	def get_context_data(self, **kwargs):
		context = super(TechnologyQueryCreateView, self).get_context_data(**kwargs)

		context['technology_details'] = Technology.objects.filter(slug=self.kwargs['technology_slug']).first()

		context['contact_background'] = Contact_BackGround.objects.filter(
            sample__exact='Contact-Background').first()

		return context