from django.contrib import admin

from .models import Technology, TechnologyElements, TechnologyTabs, TechnologyEnquiry
# Register your models here.

class MyAdmin(admin.ModelAdmin):
    """
    Model Admin Class to disable adding new objects
    """
    readonly_fields = ('sample',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Technology)
admin.site.register(TechnologyElements, MyAdmin)
admin.site.register(TechnologyTabs)
admin.site.register(TechnologyEnquiry)