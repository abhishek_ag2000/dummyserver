"""
Urls
"""
from django.conf.urls import url
from . import views

from django.views.decorators.cache import cache_page

app_name = 'blog'

urlpatterns = [

    url(r'^$', cache_page(60*15)(views.BlogListView.as_view()), name='blog_list'),

    url(r'^(?P<blog_slug>[\w-]+)/$',
        views.blog_details_view, name='blogdetail'),

    url(r'^category/(?P<blog_category_slug>[\w-]+)/$',
        views.blog_categories_details, name='blog_category_details'),

    url(r'^tag/(?P<blog_tag_slug>[\w-]+)/$',
        views.blog_tag_details, name='blog_tag_details'),

    url(r'^search/item/$',
        views.search, name='search_blog'),
]
