from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse

from .models import Blog, BlogCategories

class BlogViewSiteMap(Sitemap):
	"""
	Sitemap For Blogs Url
	"""

	def items(self):
		return ['blog:blog_list']

	def location(self, item):
		return reverse(item)


class AllBlogSiteMap(Sitemap):
	"""
	SiteMap For All Blogs
	"""
	changefreq = "always"
	priority = 0.7

	def items(self):
		return Blog.objects.all()

	def lastmod(self, obj):
		return obj.blog_title

class AllBlogCategoriesSiteMap(Sitemap):
	"""
	SiteMap For All Blog Categories
	"""
	changefreq = "always"
	priority = 0.9

	def items(self):
		return BlogCategories.objects.all()

	def lastmod(self, obj):
		return obj.title