"""
Forms
"""
from django import forms

from blog.models import BlogComments


class BlogCommentsForm(forms.ModelForm):
    """
    BlogComments Form
    """
    class Meta:
        model = BlogComments
        fields = ['name', 'email', 'comment']

    def __init__(self, *args, **kwargs):
        super(BlogCommentsForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs = {
            'placeholder' : 'Name', 'tabindex': "1", }
        self.fields['email'].widget.attrs = {
            'placeholder' : 'Email', 'tabindex': "2", }
        self.fields['comment'].widget.attrs = {
            'placeholder': 'Comment Here', 'tabindex': "3", }
