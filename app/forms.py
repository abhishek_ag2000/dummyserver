"""
Forms
"""
from django import forms

from .models import PeriodSelected, Newsletter


class DateInput(forms.DateInput):
    """
    Widgets support for date input
    """
    input_type = 'date'


class DateRangeForm(forms.ModelForm):
    """
    Date Range Form
    """

    def __init__(self, *args, **kwargs):
        super(DateRangeForm, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget.attrs = {'class': 'form-control', }
        self.fields['end_date'].widget.attrs = {'class': 'form-control', }

    class Meta:
        model = PeriodSelected
        fields = ('start_date', 'end_date')
        widgets = {
            'start_date': DateInput(),
            'end_date': DateInput(),
        }

class NewsletterForm(forms.ModelForm):
    """
    Newsletter Form
    """
    class Meta:
        model = Newsletter
        fields = ('email',)

    def __init__(self, *args, **kwargs):
        super(NewsletterForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs = {'class': 'form-control','placeholder' : 'Enter Your Email' }