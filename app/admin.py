from django.contrib import admin

from .models import Notification, OpeningModal, HeaderSliders, Seo, Keyword, PeriodSelected, CountryMaster, StateMaster, CommonInfo, CommonGallery, Landing, Trainers, Clients, TermsAndCondition, PrivacyPolicy, Testimonial, Newsletter, VideosTestimonial, CertificateSignatures, ConsultancyAndTechnologyFeedback, CourseFeedback
# Register your models here.

class MyAdmin(admin.ModelAdmin):
    """
    Model Admin Class to disable adding new objects
    """
    readonly_fields = ('sample',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class CommonGalleryAdmin(admin.TabularInline):
    """
    CommonGallery Inline Admin
    """
    model = CommonGallery
    fk_name = 'common'


class CommonInfoAdmin(admin.ModelAdmin):
    """
    Admin for CommonInfo
    """
    model = CommonInfo
    list_display = ['sample', 'email', 'phone_no', ]
    inlines = (CommonGalleryAdmin,) 
    readonly_fields = ('sample',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(OpeningModal, MyAdmin)
admin.site.register(HeaderSliders)
admin.site.register(CommonInfo, CommonInfoAdmin)
admin.site.register(Landing, MyAdmin)
admin.site.register(Trainers)
admin.site.register(Clients)
admin.site.register(TermsAndCondition, MyAdmin)
admin.site.register(PrivacyPolicy, MyAdmin)
admin.site.register(Testimonial)
admin.site.register(CountryMaster)
admin.site.register(StateMaster)
admin.site.register(PeriodSelected)
admin.site.register(Keyword)
admin.site.register(Seo, MyAdmin)
admin.site.register(Newsletter)
admin.site.register(VideosTestimonial)
admin.site.register(CertificateSignatures, MyAdmin)
admin.site.register(Notification)
admin.site.register(ConsultancyAndTechnologyFeedback)
admin.site.register(CourseFeedback)