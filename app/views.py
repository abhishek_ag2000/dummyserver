"""
Views
"""
import collections
import dateutil
from datetime import date

from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import JsonResponse
from django.contrib import messages
from django.views.generic.base import TemplateView
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Sum, Value, Count, Avg, Case, When, F
from django.views.decorators.cache import cache_page
from django.db.models.functions import Coalesce
from django.utils import timezone
from django.http import HttpResponseRedirect

from about_us.models import AboutUs, OurSkills
from blog.models import Blog
from courses.models import Courses, UserCourses
from ecommerce_cart.models import UserOrders, Coupon
from live_events.models import LiveEvents
from user_profile.models import Profile

from .common import CommonMixin, CommonAdminMixin
from .forms import DateRangeForm, NewsletterForm
from .models import OpeningModal, HeaderSliders, Landing, Trainers, TermsAndCondition, PrivacyPolicy, Testimonial, PeriodSelected, Newsletter, VideosTestimonial



def login_success(request):
    """
    Redirects users based on whether they are in the admins group
    """
    get_profile = Profile.objects.filter(user=request.user).first()

    if get_profile.user_type == 'STUDENT':
        # user is an admin
        return redirect(reverse("index_student"))
    elif request.user.is_superuser:
        return redirect("index_admin")
    else:
        return redirect("index")


@cache_page(60 * 15)
def index(request):
    """
    Index View
    """
    header_sliders = HeaderSliders.objects.all().order_by('id')[:4]

    about = AboutUs.objects.filter(sample__exact='About-Us').first()

    blog_list = Blog.objects.all().order_by('id')[:3]

    course_list = Courses.objects.filter(show_on_landing_page=True).order_by('-id')[:5]

    landing = Landing.objects.filter(sample__exact='LANDING').first()

    trainers_list = Trainers.objects.all().order_by('id')

    review_list = Testimonial.objects.all().order_by('id')

    upcoming_events = LiveEvents.objects.all().order_by('-id')[:3]

    opening_modal = OpeningModal.objects.filter(sample__exact='OPENING-MODAL', display_modal=True).first()

    if request.method == "POST":
        news_form = NewsletterForm(request.POST or None)
        if news_form.is_valid():
            email = request.POST.get('email')
            news = Newsletter.objects.create(email=email)
            news.save()
            messages.success(
                            request, 'Your request is successfully submitted! Thank You')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
    else:
        news_form = NewsletterForm()

    video_list = VideosTestimonial.objects.all().order_by('-id')[:20]

    skills_list = OurSkills.objects.filter(about=about).order_by('-id')[:4]

    if request.user.is_authenticated:
        #### Courses Removal on expiry

        # Query all the User Courses in our database
        user_courses = UserCourses.objects.filter(user=request.user)

        if user_courses:
        # Iterate through them
            for item in user_courses:
                if item.date_of_expiry < timezone.now():

                    # If the expiration date is bigger than first remove user from course user list
                    item.course.subscribed_users.remove(item.user)
                    item.course.save()

                    # Then Delete the User Course Model
                    item.delete()


    context = {

    	'header_sliders' : header_sliders,
    	'about' : about,
        'blog_list' : blog_list,
        'course_list' : course_list,
        'trainers_list' : trainers_list,
        'landing' : landing,
        'review_list' : review_list,
        'upcoming_events' : upcoming_events,
        'opening_modal' : opening_modal,
        'news_form' : news_form,
        'video_list' : video_list,
        'skills_list' : skills_list,
    }

    return CommonMixin.render(request, 'index.html',context)

@login_required
@cache_page(60 * 15)
def index_admin(request):
    """
    Index Admin View(Educasta Dashboard)
    """

    if not request.user.is_superuser:
        messages.error(request, 'You have no permission to access the requested resource!')
        return redirect(reverse('index_student'))

    total_sales = UserOrders.objects.all().aggregate(
            the_sum=Coalesce(Sum('grand_total'), Value(0)))['the_sum']

    students_count = Profile.objects.filter(user_type='STUDENT').aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum']

    total_orders = UserOrders.objects.all().aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum']

    average_sales = UserOrders.objects.all().aggregate(
            the_sum=Coalesce(Avg('grand_total'), Value(0)))['the_sum']

    period_selected = PeriodSelected.objects.filter(user=request.user).first()

    results = collections.OrderedDict()
    sales_inventory_result = UserOrders.objects.filter(date_ordered__gte=period_selected.start_date, date_ordered__lte=period_selected.end_date).annotate(
        real_total=Case(When(grand_total__isnull=True, then=0), default=F('grand_total')))

    date_cursor = period_selected.start_date

    while date_cursor < period_selected.end_date:

        month_partial_sales_inventory = sales_inventory_result.filter(date_ordered__month=date_cursor.month, date_ordered__year=date_cursor.year).aggregate(
                partial_total=Coalesce(Sum('real_total'), Value(0)))['partial_total']

        results[(date_cursor.month, date_cursor.year)] = [
                month_partial_sales_inventory]

        date_cursor += dateutil.relativedelta.relativedelta(months=1)

    coupon_list = Coupon.objects.filter(valid_till__gte=date.today()).order_by('-id')

    new_student_list = Profile.objects.filter(user_type='STUDENT').order_by('-id')[:10]

    top_courses_list = Courses.objects.annotate(
         total_sales=Coalesce(Sum('subscribed_courses__total'), 0), 
         quantity_sales=Coalesce(Count('subscribed_courses__id'), 0),
        ).order_by('-quantity_sales')[:10]

    user_list = Profile.objects.filter(user__is_active=True).exclude(user=request.user).order_by("user__username")

    page = request.GET.get('page', 1)

    paginator = Paginator(user_list, 15)

    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages) 

    if request.user.is_authenticated:
        #### Courses Removal on expiry

        # Query all the User Courses in our database
        user_courses = UserCourses.objects.filter(user=request.user)

        # Iterate through them
        for item in user_courses:
            if item.date_of_expiry < timezone.now():

                # If the expiration date is bigger than first remove user from course user list
                item.course.subscribed_users.remove(item.user)
                item.course.save()

                # Then Delete the User Course Model
                item.delete()   


    context = {
        'total_sales' : total_sales,
        'students_count' : students_count,
        'total_orders' : total_orders,
        'average_sales' : average_sales,
        'period_selected' : period_selected,
        'results' : results.items(),
        'coupon_list' : coupon_list,
        'new_student_list' : new_student_list,
        'top_courses_list' : top_courses_list,
        'user_list' : users
    }
    return CommonAdminMixin.render(request, 'educasta/index.html', context)

@login_required
def index_student(request):
    """
    Student Dashboard View
    """
    student_courses = UserCourses.objects.filter(user=request.user).order_by('-id')[:5]

    browser_family = ['Mobile Safari', 'Safari']


    context = {
        'student_courses' : student_courses,
        'browser_family' : browser_family
    }
    return CommonAdminMixin.render(request, 'educasta/student_index.html', context)

@cache_page(60 * 15)
def terms_view(request):
    """
    Terms View
    """
    terms_details = TermsAndCondition.objects.filter(sample__exact='Terms-And-Condition').first()
    context = {
        'terms_details' : terms_details
    }
    return CommonMixin.render(request, 'terms.html', context)

@cache_page(60 * 15)
def privacy_view(request):
    """
    Terms View
    """
    privacy_details = PrivacyPolicy.objects.filter(sample__exact='PRIVACY-POLICY').first()
    
    context = {
        'privacy_details' : privacy_details
    }
    return CommonMixin.render(request, 'privacy.html', context)



def period_selected_update(request):
    """
    Period selection (Update View)
    """
    data = {'is_error': False, 'error_message': ""}

    if not request.user.is_authenticated:
        data['is_error'] = True
        data['error_message'] = "Login Failed!"
        return JsonResponse(data)

    period_selected = PeriodSelected.objects.filter(user=request.user).first()

    if request.method == 'POST':
        form = DateRangeForm(request.POST, instance=period_selected)

        if form.is_valid():
            form.save()
        else:
            data['is_error'] = True
            data['error_message'] = "Please rectify the form error and then try again"
    else:
        form = DateRangeForm(instance=period_selected)

    context = {
        'form': form
    }
    data['html_form'] = render_to_string(
        'selectdate_update.html', context, request=request)

    return JsonResponse(data)


class AdminShortcuts(TemplateView):
    """
    All Admin Shortcuts
    """
    template_name = 'educasta/admin_shortcuts.html'