import datetime
from PIL import Image
import sys
import os
from io import BytesIO
from django.db import models
from django.utils import timezone
from django.conf import settings
from ckeditor_uploader.fields import RichTextUploadingField
from django.db.models.signals import post_save
from django.dispatch import receiver
from embed_video.fields import EmbedVideoField
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.validators import URLValidator, RegexValidator
# Create your models here.

def file_size(value):  # add this to some file where you can import it from
	"""
	Function to validate file size
	"""
	MAX_UPLOAD_SIZE = 20971520
	if value.size > MAX_UPLOAD_SIZE:
		raise ValidationError('File too large. Size should not exceed 20 MB.')

class OpeningModal(models.Model):
	"""
	Opening Modal
	"""
	sample = models.CharField(max_length=50)
	image = models.ImageField(
		upload_to='modal_image', null=True, blank=True, help_text='Image Size 500 x 500')
	header_text = models.CharField(max_length=200)
	text = models.TextField(blank=True, null=True)
	display_modal = models.BooleanField(default=False)

	def __str__(self):
		return self.sample

	def save(self, *args, **kwargs):

		if self.image:
			temp_image = Image.open(self.image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((500, 500))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)

		super(OpeningModal, self).save(*args, **kwargs)


class CommonInfo(models.Model):
	"""
	Common Info
	"""
	sample = models.CharField(max_length=200)
	logo = models.ImageField(
		upload_to='logo', null=True, blank=True, help_text='Image Size 258 x 44, PNG Image')
	facebook_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	google_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	twitter_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	instagram_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	whatsapp_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	youtube_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	linkedin_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	address = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	email = models.EmailField(max_length=100)
	phone_regex = RegexValidator(
		regex=r'^\+?1?\d{6,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
	# validators should be a list
	phone_no = models.CharField(
		validators=[phone_regex], max_length=17, null=True)
	footer_about_us_text = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	footer_image = models.ImageField(
		upload_to='footer_background', null=True, blank=True, help_text='Image Size 1920 x 506')

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.sample

	def save(self, *args, **kwargs):

		if self.footer_image:
			temp_image = Image.open(self.footer_image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((1920, 506))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.footer_image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.footer_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)

		if self.logo:
			temp_image = Image.open(self.logo).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((258, 44))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.logo = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.logo.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)

		super(CommonInfo, self).save(*args, **kwargs)

class CommonGallery(models.Model):
	"""
	Common Gallery Model
	"""
	common = models.ForeignKey(
        CommonInfo, on_delete=models.CASCADE, related_name='common_galleries')
	image = models.ImageField(
		upload_to='common_gallery_image', null=True, blank=True, help_text='Image Size 80 x 70, PNG Image')

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.common.sample

	def save(self, *args, **kwargs):
        
		if self.image:
			temp_image = Image.open(self.image).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((80, 70))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.image = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.image.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)

		super(CommonGallery, self).save(*args, **kwargs)
	

class HeaderSliders(models.Model):
	"""
	Heading Slider
	"""
	header_text = models.CharField(max_length=200)
	description = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	background_image = models.ImageField(
		upload_to='header_background', null=True, blank=True, help_text='Image Size 1920 x 845')
	display_image = models.ImageField(
		upload_to='header_display', null=True, blank=True, help_text='Image Size 504 x 787, PNG Format')

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.header_text

	def save(self, *args, **kwargs):

		if self.background_image:
			temp_image = Image.open(self.background_image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((1920, 845))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.background_image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.background_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)
        
		if self.display_image:
			temp_image = Image.open(self.display_image).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((504, 787))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.display_image = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.display_image.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)

		super(HeaderSliders, self).save(*args, **kwargs)

class Landing(models.Model):
	"""
	Landing Model
	"""
	sample = models.CharField(max_length=200)
	course_background = models.ImageField(
		upload_to='course_background', null=True, blank=True, help_text='Image Size 1920 x 919')
	course_heading = models.CharField(max_length=200, blank=True, null=True)
	course_sub_heading = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	teacher_heading = models.CharField(max_length=200, blank=True, null=True)
	teacher_sub_heading = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	achievement_heading = models.CharField(max_length=200, blank=True, null=True)
	achievement_sub_heading = RichTextUploadingField(
		blank=True, null=True, config_name='special')

	achievement_text_1 = models.CharField(max_length=200, blank=True, null=True)
	achievement_count_1 = models.IntegerField(default=0)

	achievement_text_2 = models.CharField(max_length=200, blank=True, null=True)
	achievement_count_2 = models.IntegerField(default=0)

	achievement_text_3 = models.CharField(max_length=200, blank=True, null=True)
	achievement_count_3 = models.IntegerField(default=0)

	achievement_text_4 = models.CharField(max_length=200, blank=True, null=True)
	achievement_count_4 = models.IntegerField(default=0)

	live_session_heading = models.CharField(max_length=200, blank=True, null=True)
	live_session_sub_heading = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	live_session_background = models.ImageField(
		upload_to='live_session_background', null=True, blank=True, help_text='Image Size 1920 x 880')

	scholarship_heading_1 = models.CharField(max_length=100, blank=True, null=True)
	scholarship_text_1 = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	scholarship_image_1 = models.ImageField(
		upload_to='scholarship_image_1', null=True, blank=True, help_text='Image Size 60 x 67, PNG Image')

	scholarship_heading_2 = models.CharField(max_length=100, blank=True, null=True)
	scholarship_text_2 = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	scholarship_image_2 = models.ImageField(
		upload_to='scholarship_image_2', null=True, blank=True, help_text='Image Size 60 x 67, PNG Image')

	scholarship_heading_3 = models.CharField(max_length=100, blank=True, null=True)
	scholarship_text_3 = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	scholarship_image_3 = models.ImageField(
		upload_to='scholarship_image_3', null=True, blank=True, help_text='Image Size 60 x 67, PNG Image')

	scholarship_heading_4 = models.CharField(max_length=100, blank=True, null=True)
	scholarship_text_4 = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	scholarship_image_4 = models.ImageField(
		upload_to='scholarship_image_4', null=True, blank=True, help_text='Image Size 60 x 67, PNG Image')

	def __str__(self):
		return self.sample

	def save(self, *args, **kwargs):

		if self.course_background:
			temp_image = Image.open(self.course_background).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((1920, 920))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.course_background = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.course_background.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)
       
		if self.live_session_background:
			temp_image = Image.open(self.live_session_background).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((1920, 880))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.live_session_background = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.live_session_background.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)

		if self.scholarship_image_1:
			temp_image = Image.open(self.scholarship_image_1).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((60, 67))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.scholarship_image_1 = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.scholarship_image_1.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)

		if self.scholarship_image_3:
			temp_image = Image.open(self.scholarship_image_3).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((60, 67))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.scholarship_image_3 = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.scholarship_image_3.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)

		if self.scholarship_image_2:
			temp_image = Image.open(self.scholarship_image_2).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((60, 67))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.scholarship_image_2 = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.scholarship_image_2.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)

		if self.scholarship_image_4:
			temp_image = Image.open(self.scholarship_image_4).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((60, 67))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.scholarship_image_4 = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.scholarship_image_4.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)


		super(Landing, self).save(*args, **kwargs)

class Trainers(models.Model):
	"""
	Trainers Model
	"""
	name = models.CharField(max_length=200)
	designation = models.CharField(max_length=200)
	description = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	past_experience = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	image = models.ImageField(
		upload_to='trainers_image', null=True, blank=True, help_text='Image Size 200 x 256')
	facebook_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	google_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	twitter_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	youtube_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):

		if self.image:
			temp_image = Image.open(self.image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((200, 256))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)
       

		super(Trainers, self).save(*args, **kwargs)

class Clients(models.Model):
	"""
	Clients Model
	"""
	name = models.CharField(max_length=200)
	logo_image = models.ImageField(
		upload_to='client_logo', null=True, blank=True, help_text='Image Size 78 x 84, PNG Format')

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):

		if self.logo_image:
			temp_image = Image.open(self.logo_image).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((78, 84))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.logo_image = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.logo_image.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)
       

		super(Clients, self).save(*args, **kwargs)


class TermsAndCondition(models.Model):
	"""
	Terms and Condition Model
	"""
	sample = models.CharField(max_length=200)
	text = RichTextUploadingField(
		blank=True, null=True, config_name='special')

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.sample


class PrivacyPolicy(models.Model):
	"""
	Terms and Condition Model
	"""
	sample = models.CharField(max_length=200)
	text = RichTextUploadingField(
		blank=True, null=True, config_name='special')

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.sample


class Testimonial(models.Model):
	"""
	Testimonial Model
	"""
	name = models.CharField(max_length=200)
	review = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	client_image = models.ImageField(
		upload_to='client_image', null=True, blank=True, help_text='Image Size 585 x 450')
	background_image = models.ImageField(
		upload_to='client/background_image', null=True, blank=True, help_text='Image Size 585 x 450')

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		if self.client_image:
			temp_image = Image.open(self.client_image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((585, 450))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.client_image = InMemoryUploadedFile(output_io_stream,
			                                         'ImageField', "%s.jpg" % self.client_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)
		if self.background_image:
			temp_image = Image.open(self.background_image).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((585, 450))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.background_image = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.background_image.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)

		super(Testimonial, self).save(*args, **kwargs)


class ConsultancyAndTechnologyFeedback(models.Model):
	"""
	Testimonial Model
	"""
	name = models.CharField(max_length=200)
	review = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	client_image = models.ImageField(
		upload_to='client_image', null=True, blank=True, help_text='Image Size 585 x 450')
	background_image = models.ImageField(
		upload_to='client/background_image', null=True, blank=True, help_text='Image Size 585 x 450')

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		if self.client_image:
			temp_image = Image.open(self.client_image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((585, 450))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.client_image = InMemoryUploadedFile(output_io_stream,
			                                         'ImageField', "%s.jpg" % self.client_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)
		if self.background_image:
			temp_image = Image.open(self.background_image).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((585, 450))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.background_image = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.background_image.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)

		super(ConsultancyAndTechnologyFeedback, self).save(*args, **kwargs)

class CourseFeedback(models.Model):
	"""
	CourseFeedback Model
	"""
	name = models.CharField(max_length=200)
	review = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	client_image = models.ImageField(
		upload_to='client_image', null=True, blank=True, help_text='Image Size 585 x 450')
	background_image = models.ImageField(
		upload_to='client/background_image', null=True, blank=True, help_text='Image Size 585 x 450')

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		if self.client_image:
			temp_image = Image.open(self.client_image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((585, 450))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.client_image = InMemoryUploadedFile(output_io_stream,
			                                         'ImageField', "%s.jpg" % self.client_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)
		if self.background_image:
			temp_image = Image.open(self.background_image).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((585, 450))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.background_image = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.background_image.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)

		super(CourseFeedback, self).save(*args, **kwargs)


class CountryMaster(models.Model):
	"""
	Country Master
	"""
	country_name = models.CharField(max_length=30, unique=True)

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.country_name


class StateMaster(models.Model):
	"""
	State Master
	"""
	state_name = models.CharField(max_length=30, unique=True)
	state_code = models.CharField(max_length=2, unique=True)

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.state_name

class PeriodSelected(models.Model):
    """
    Period selected by a user
    """
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="auth_user_period", on_delete=models.CASCADE)
    start_date = models.DateField(default=timezone.now)
    end_date = models.DateField(default=timezone.now)

    def __str__(self):
        return str(self.user.username) + " [From " + str(self.start_date) + " To " + str(self.end_date) + "]"

    def clean(self):
        if self.start_date > self.end_date:
            raise ValidationError({
                'start_date': ["Start Date cannot be greater than End Date"],
                'end_date': ["End Date cannot be earlier than Start Date"]
            })

class Keyword(models.Model):
	"""
	Keyword Model
	"""
	name = models.CharField(max_length=255)

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.name


class Seo(models.Model):
	"""
	SEO  - Single meta data
	"""
	sample = models.CharField(max_length=100)
	MetaDescription = models.TextField(null=True, blank=True)
	FocusKeyword = models.ManyToManyField(
		Keyword, related_name='related_keywords', blank=True) 

	class Meta:
		app_label = 'app'

	def __str__(self):
		return self.sample

class Newsletter(models.Model):
	"""
	Newsletter Model
	"""
	email = models.EmailField(max_length=100)

	def __str__(self):
		return self.email

class VideosTestimonial(models.Model):
	"""
	Review Video Model
	"""
	video_name = models.CharField(max_length=200)
	youtube_video_link = EmbedVideoField()  # same like models.URLField()

	class Meta:
		app_label = 'app'

class CertificateSignatures(models.Model):
	"""
	Certificate Signature
	"""
	sample = models.CharField(max_length=200)
	ceo_signature = models.ImageField(
		upload_to='signature_image', null=True, blank=True, help_text='Image Size 100 x 100, PNG Image')
	co_founder_signature = models.ImageField(
		upload_to='signature_image', null=True, blank=True, help_text='Image Size 100 x 100, PNG Image')

	def __str__(self):
		return self.sample

	def save(self, *args, **kwargs):
		if self.ceo_signature:
			temp_image = Image.open(self.ceo_signature).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((100, 100))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.ceo_signature = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.ceo_signature.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)

		if self.co_founder_signature:
			temp_image = Image.open(self.co_founder_signature).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((100, 100))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.co_founder_signature = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.co_founder_signature.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)

		super(CertificateSignatures, self).save(*args, **kwargs)

class Notification(models.Model):
	"""
	Notification Model
	"""
	user = models.ForeignKey(
		settings.AUTH_USER_MODEL, related_name='user_notification', on_delete=models.CASCADE, null=True)
	notification_for = models.CharField(max_length=200, null=True)
	text = models.TextField()
	date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.text



# @receiver(post_save, sender=settings.AUTH_USER_MODEL)
# # sender, instance, created, **kwargs
# def create_period_on_user_create(instance, created, **kwargs):
#     """
#     Signal to create a Period whenever a user Registers.
#     """
#     PeriodSelected.objects.update_or_create(user=instance, defaults={
#                                      'start_date': datetime.date((datetime.datetime.now().year), 4, 1), 
#                                      'end_date': datetime.date((datetime.datetime.now().year) + 1, 3, 31)})