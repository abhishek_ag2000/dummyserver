from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse

class HomeViewSiteMap(Sitemap):
	"""
	Sitemap For Home Url
	"""

	def items(self):
		return ['index']

	def location(self, item):
		return reverse(item)