"""
Common mixin and decorators
"""
from django.views import generic
from django.shortcuts import render
from django.db.models import Sum, Value, Count
from django.db.models.functions import Coalesce

from consultancy.models import ConsultancyServices
from courses.models import CourseCategory, CoursesPackages
from ecommerce_cart.models import CartItems, UserOrders
from technology.models import Technology

from .models import CommonInfo, Clients, Seo, Notification


class CommonMixin(generic.base.ContextMixin):
    """
Common Mixin (used for footer elements)
"""
    @staticmethod
    def update_common_context(context, user):
        """
        Updates Common context for footer elements
        """
        if not user or not user.is_authenticated:
            # user invalid; setting default for non auth user
            context['cart_items'] = None
            context['cart_total'] = 0
            context['cart_count'] = 0
            context['browser_family'] = ['Mobile Safari', 'Safari']

            context['common_info'] = CommonInfo.objects.filter(sample__exact='Common-Info').first()

            context['client_list'] = Clients.objects.all().order_by('id')

            context['course_category_list'] = CourseCategory.objects.all().order_by('ordering_id')

            context['service_list'] = ConsultancyServices.objects.all().order_by('-id')[:10]

            context['tech_list'] = Technology.objects.all().order_by('-id')[:10]

            context['seo'] = Seo.objects.filter(sample__exact='COMMON-SEO').first()
            context['order_count'] = 0
            return

            
        context['browser_family'] = ['Mobile Safari', 'Safari']

        context['common_info'] = CommonInfo.objects.filter(sample__exact='Common-Info').first()

        context['client_list'] = Clients.objects.all().order_by('id')

        context['course_category_list'] = CourseCategory.objects.all().order_by('ordering_id')

        context['seo'] = Seo.objects.filter(sample__exact='COMMON-SEO').first()

        context['service_list'] = ConsultancyServices.objects.all().order_by('-id')[:10]

        context['tech_list'] = Technology.objects.all().order_by('-id')[:10]


        context['cart_items'] = CartItems.objects.filter(
            user=user, is_ordered=False)

        context['cart_total'] = context['cart_items'].aggregate(
            the_sum=Coalesce(Sum('cart_total'), Value(0)))['the_sum']

        context['cart_count'] = context['cart_items'].aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum']  # Cart Item Count

        context['order_count'] = UserOrders.objects.filter(user=user).aggregate(
            the_sum=Coalesce(Count('id'), Value(0)))['the_sum']  # Cart Item Count



    def get_context_data(self, **kwargs):
        context = super(CommonMixin, self).get_context_data(**kwargs)
        CommonMixin.update_common_context(context, self.request.user)
        return context

    @staticmethod
    def render(request, template, context):
        """
        Updates common context and render using the template supplied(Mainly used for Function-Based Views)
        """
        CommonMixin.update_common_context(context, request.user)
        return render(request, template, context)



class CommonAdminMixin(generic.base.ContextMixin):
    """
Common Mixin (used for footer elements)
"""
    @staticmethod
    def update_common_context(context, user):
        """
        Updates Common context for footer elements
        """

        context['notification_list'] = Notification.objects.filter(user=user).order_by('-id')[:10]

            

    def get_context_data(self, **kwargs):
        context = super(CommonAdminMixin, self).get_context_data(**kwargs)
        CommonAdminMixin.update_common_context(context, self.request.user)
        return context

    @staticmethod
    def render(request, template, context):
        """
        Updates common context and render using the template supplied(Mainly used for Function-Based Views)
        """
        CommonAdminMixin.update_common_context(context, request.user)
        return render(request, template, context)
