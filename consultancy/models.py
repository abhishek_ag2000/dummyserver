from PIL import Image
import sys
import os
from io import BytesIO
from django.db import models
from django.core.validators import URLValidator, RegexValidator
from django.utils.text import slugify
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.files.uploadedfile import InMemoryUploadedFile
from embed_video.fields import EmbedVideoField

# Create your models here.

class ConsultancyElements(models.Model):
	"""
	Consultancy Element Changes
	"""
	sample = models.CharField(max_length=100)
	heading = models.TextField()
	text = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	quote = models.TextField(blank=True, null=True)
	image = models.ImageField(
		upload_to='service_image_common', null=True, blank=True, help_text='Image Size 830 x 372')
	about_us_image = models.ImageField(
		upload_to='about_image_service', null=True, blank=True, help_text='Image Size 268 x 99')

	class Meta:
		app_label = 'consultancy'

	def __str__(self):
		return self.sample

	def save(self, *args, **kwargs):

		if self.image:
			temp_image = Image.open(self.image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((830, 372))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)

		if self.about_us_image:
			temp_image = Image.open(self.about_us_image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((268, 99))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.about_us_image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.about_us_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)

		super(ConsultancyElements, self).save(*args, **kwargs)

class ConsultancyServices(models.Model):
	"""
	Consultancy Services Model
	"""
	name = models.CharField(max_length=200)
	short_description = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	detailed_description = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	image = models.ImageField(
		upload_to='service_image', null=True, blank=True, help_text='Image Size 1030 x 527')
	slug = models.SlugField(
        max_length=200, unique=True, null=True, blank=True)

	class Meta:
		app_label = 'consultancy'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):

		self.slug = slugify(self.name)

		if self.image:
			temp_image = Image.open(self.image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((1030, 527))
			temp_resized_image.save(
			    output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.image = InMemoryUploadedFile(output_io_stream,
			                                       'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)

		super(ConsultancyServices, self).save(*args, **kwargs)

class ConsultancyTabs(models.Model):
	"""
	Consultancy Tabs
	"""
	consultancy = models.ForeignKey(
        ConsultancyServices, on_delete=models.CASCADE, related_name='service_tabs', null=True)
	name = models.CharField(max_length=200)
	text = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	slug = models.SlugField(
        max_length=200, unique=True, null=True, blank=True)

	class Meta:
		app_label = 'consultancy'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):

		self.slug = slugify(self.name)

		super(ConsultancyTabs, self).save(*args, **kwargs)


class ConsultancyVideos(models.Model):
	"""
	Consultancy Videos Model
	"""
	service = models.ForeignKey(
        ConsultancyServices, on_delete=models.CASCADE, related_name='service_videos')
	youtube_video_link = EmbedVideoField()  # same like models.URLField()

	class Meta:
		app_label = 'consultancy'

class ConsultancyEnquiry(models.Model):
	"""
	Consultancy Enquiry Model
	"""
	consultancy = models.ForeignKey(
        ConsultancyServices, on_delete=models.CASCADE, related_name='services')
	name = models.CharField(max_length=100)
	email = models.EmailField(max_length=100)
	phone_regex = RegexValidator(
		regex=r'^\+?1?\d{6,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
	# validators should be a list
	phone_no = models.CharField(
		validators=[phone_regex], max_length=17, null=True)
	subject = models.CharField(max_length=200)
	message = models.TextField()

	class Meta:
		app_label = 'consultancy'

	def __str__(self):
		return self.name