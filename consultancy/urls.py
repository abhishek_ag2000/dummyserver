"""
Consultancy (URLs)
"""
from django.conf.urls import url
from . import views

from django.views.decorators.cache import cache_page

app_name = 'consultancy'

urlpatterns = [
	
	url(r'^$', cache_page(60*15)(views.ConsultancyServicesListView.as_view()), name='services'),

	url(r'^(?P<service_slug>[\w-]+)/details/$',
        cache_page(60*15)(views.ConsultancyDetailsView.as_view()), name='service-detail'),

	url(r'^enquiry/(?P<service_slug>[\w-]+)/$',
        cache_page(60*15)(views.ConsultancyQueryCreateView.as_view()), name='service_enquiry'),

]