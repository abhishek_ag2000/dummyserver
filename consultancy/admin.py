from django.contrib import admin

from .models import ConsultancyServices, ConsultancyVideos, ConsultancyElements, ConsultancyTabs, ConsultancyEnquiry
# Register your models here.


class MyAdmin(admin.ModelAdmin):
    """
    Model Admin Class to disable adding new objects
    """
    readonly_fields = ('sample',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class ConsultancyVideosAdmin(admin.TabularInline):
	"""
	ConsultancyVideos Inline Admin
	"""
	model = ConsultancyVideos
	fk_name = 'service'


class ConsultancyAdmin(admin.ModelAdmin):
	"""
	ConsultancyServices Admin
	"""
	model = ConsultancyServices
	inlines = (ConsultancyVideosAdmin,) 

admin.site.register(ConsultancyElements, MyAdmin)
admin.site.register(ConsultancyServices, ConsultancyAdmin)
admin.site.register(ConsultancyTabs)
admin.site.register(ConsultancyEnquiry)