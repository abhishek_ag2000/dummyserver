from django.shortcuts import render
from django.contrib import messages
from django.conf import settings
from django.urls import reverse
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.views.generic import ListView, DetailView, CreateView

from about_us.models import AboutUs
from app.common import CommonMixin
from app.models import ConsultancyAndTechnologyFeedback
from contact_us.models import Contact_BackGround

from .forms import ConsultancyEnquiryForm
from .models import ConsultancyServices, ConsultancyVideos, ConsultancyElements, ConsultancyTabs, ConsultancyEnquiry
# Create your views here.


def send_service_query_message_query_mail(contact):
    """
    Send Service Contact Mail
    """
    subject = contact.subject

    plaintext = get_template('query_from_service/query.txt')

    htmly     = get_template('query_from_service/query.html')

    context = { 'contact' : contact }

    from_email = settings.DEFAULT_FROM_EMAIL

    text_content = plaintext.render(context)
    html_content = htmly.render(context)

    msg = EmailMultiAlternatives(subject, text_content, from_email, ['learning@ankrglobal.com'])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


class ConsultancyServicesListView(CommonMixin, ListView):
	"""
	ConsultancyServices List View
	"""
	model = ConsultancyServices
	context_object_name = 'service_list'
	template_name = 'service_list.html'

	def get_queryset(self):
		return self.model.objects.all().order_by('-id')

	def get_context_data(self, **kwargs):
		context = super(ConsultancyServicesListView, self).get_context_data(**kwargs)

		context['service_videos'] = ConsultancyVideos.objects.all().order_by('-id')[:10]

		context['review_list'] = ConsultancyAndTechnologyFeedback.objects.all().order_by('id')

		context['service_elements'] = ConsultancyElements.objects.filter(sample__exact='SERVICE-ELEMENTS').first()

		context['about'] = AboutUs.objects.filter(sample__exact='About-Us').first()

		return context

class ConsultancyDetailsView(CommonMixin, DetailView):
	"""
	Consultancy Details View
	"""
	context_object_name = 'service_details'
	model = ConsultancyServices
	template_name = 'service_details.html'

	def get_object(self):
		service_details = ConsultancyServices.objects.filter(slug=self.kwargs['service_slug']).first()
		return service_details

	def get_context_data(self, **kwargs):
		context = super(ConsultancyDetailsView, self).get_context_data(**kwargs)

		service_details = self.get_object()

		context['service_videos'] = ConsultancyVideos.objects.filter(service=service_details).order_by('-id')

		context['service_list'] = ConsultancyServices.objects.all().order_by('-id')

		context['tab_list'] = ConsultancyTabs.objects.filter(consultancy=service_details).order_by('id')

		return context

class ConsultancyQueryCreateView(CommonMixin, CreateView):
	"""
	ContactUsQuery Create View
	"""
	form_class = ConsultancyEnquiryForm
	template_name = "consultancy_enquiry.html"

	def get_success_url(self, **kwargs):
		consultancy_details = ConsultancyServices.objects.filter(slug=self.kwargs['service_slug']).first()
		consultancy_enquiry = ConsultancyEnquiry.objects.filter(id=self.object.id).first()
		send_service_query_message_query_mail(contact=consultancy_enquiry)
		messages.success(
	    	self.request, 'Send successfully! Our Team will get in touch with you.')
		return reverse('consultancy:service_enquiry', kwargs={'service_slug': consultancy_details.slug})

	def form_valid(self, form):
		consultancy_details = ConsultancyServices.objects.filter(slug=self.kwargs['service_slug']).first()
		form.instance.consultancy = consultancy_details
		return super(ConsultancyQueryCreateView, self).form_valid(form)

	def get_context_data(self, **kwargs):
		context = super(ConsultancyQueryCreateView, self).get_context_data(**kwargs)

		context['consultancy_details'] = ConsultancyServices.objects.filter(slug=self.kwargs['service_slug']).first()

		context['contact_background'] = Contact_BackGround.objects.filter(
            sample__exact='Contact-Background').first()

		return context

