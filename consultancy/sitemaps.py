from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse

class ConsultancySiteMap(Sitemap):
	"""
	Sitemap For Consultancy Us Url
	"""

	def items(self):
		return ['consultancy:services']

	def location(self, item):
		return reverse(item)