from PIL import Image
import sys
import os
from io import BytesIO

from django.db import models
from django.shortcuts import reverse
from django.utils.text import slugify
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.validators import URLValidator, RegexValidator
# Create your models here.


class LiveEvents(models.Model):
	"""
	Live Events Model
	"""
	event_name = models.CharField(max_length=200)
	event_description = RichTextUploadingField(
        blank=True, null=True, config_name='special')
	event_image = models.ImageField(
        upload_to='event_image', null=True, blank=True, help_text='Image Size 830 x 320')
	event_date = models.DateTimeField(null=True)
	place_of_event = models.CharField(max_length=200,blank=True,null=True)
	facebook_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	google_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	instagram_link = models.TextField(
		validators=[URLValidator()], null=True, blank=True)
	slug = models.SlugField(
        max_length=255, unique=True, null=True, blank=True)
	button_name = models.CharField(max_length=100, blank=True, null=True)
	button_url = models.TextField(
		validators=[URLValidator()], null=True, blank=True)

	class Meta:
		app_label = 'live_events'

	def __str__(self):
		return self.event_name

	def save(self, *args, **kwargs):

		self.slug = slugify(self.event_name)

		if self.event_image:
			temp_image = Image.open(self.event_image).convert('RGB')
			output_io_stream = BytesIO()
			temp_resized_image = temp_image.resize((830, 320))
			temp_resized_image.save(
				output_io_stream, format='JPEG', quality=60)
			output_io_stream.seek(0)
			self.event_image = InMemoryUploadedFile(output_io_stream,
			                                   'ImageField', "%s.jpg" % self.event_image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output_io_stream), None)
		super(LiveEvents, self).save(*args, **kwargs)

class LiveEventsTabs(models.Model):
	"""
	LiveEvents Tabs
	"""
	liveevents = models.ForeignKey(
        LiveEvents, on_delete=models.CASCADE, related_name='live_tabs', null=True)
	name = models.CharField(max_length=200)
	text = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	slug = models.SlugField(
        max_length=200, unique=True, null=True, blank=True)

	class Meta:
		app_label = 'live_events'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):

		self.slug = slugify(self.name)

		super(LiveEventsTabs, self).save(*args, **kwargs)


class LiveEventsEnquiry(models.Model):
	"""
	LiveEvents Enquiry Model
	"""
	liveevents = models.ForeignKey(
        LiveEvents, on_delete=models.CASCADE, related_name='events')
	name = models.CharField(max_length=100)
	email = models.EmailField(max_length=100)
	phone_regex = RegexValidator(
		regex=r'^\+?1?\d{6,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
	# validators should be a list
	phone_no = models.CharField(
		validators=[phone_regex], max_length=17, null=True)
	subject = models.CharField(max_length=200)
	message = models.TextField()

	class Meta:
		app_label = 'live_events'

	def __str__(self):
		return self.name