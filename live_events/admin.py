from django.contrib import admin

from .models import LiveEvents, LiveEventsTabs, LiveEventsEnquiry
# Register your models here.

class LiveEventsTabsAdmin(admin.TabularInline):
	"""
	LiveEventsTabs Inline Admin
	"""
	model = LiveEventsTabs
	fk_name = 'liveevents'



class LiveEventsAdmin(admin.ModelAdmin):
    """
    Admin for LiveEvents
    """
    model = LiveEvents
    list_display = ['event_name', 'event_date',]
    inlines = (LiveEventsTabsAdmin,) 


admin.site.register(LiveEvents, LiveEventsAdmin)
admin.site.register(LiveEventsEnquiry)