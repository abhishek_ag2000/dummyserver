from django.shortcuts import render
from django.contrib import messages
from django.conf import settings
from django.urls import reverse
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.views.generic import ListView, DetailView, CreateView

from app.common import CommonMixin
from courses.models import CourseCategory
from contact_us.models import Contact_BackGround

from .forms import LiveEventEnquiryForm
from .models import LiveEvents, LiveEventsTabs, LiveEventsEnquiry
# Create your views here.


def send_event_query_message_query_mail(contact):
    """
    Send Service Contact Mail
    """
    subject = contact.subject

    plaintext = get_template('query_from_events/query.txt')

    htmly     = get_template('query_from_events/query.html')

    context = { 'contact' : contact }

    from_email = settings.DEFAULT_FROM_EMAIL

    text_content = plaintext.render(context)
    html_content = htmly.render(context)

    msg = EmailMultiAlternatives(subject, text_content, from_email, ['learning@ankrglobal.com'])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

class LiveEventsListView(CommonMixin, ListView):
	"""
	LiveEvents List View
	"""
	model = LiveEvents
	context_object_name = 'event_list'
	template_name = 'event_list.html'
	paginate_by = 6

	def get_queryset(self):
		return self.model.objects.all().order_by('-id')

	# def get_context_data(self, **kwargs):
	# 	context = super(ConsultancyServicesListView, self).get_context_data(**kwargs)

	# 	context['service_videos'] = ConsultancyVideos.objects.all().order_by('-id')[:10]

	# 	return context

class LiveEventsDetailsView(CommonMixin, DetailView):
	"""
	Consultancy Details View
	"""
	context_object_name = 'event_details'
	model = LiveEvents
	template_name = 'event_details.html'

	def get_object(self):
		event_details = LiveEvents.objects.filter(slug=self.kwargs['event_slug']).first()
		return event_details

	def get_context_data(self, **kwargs):
		context = super(LiveEventsDetailsView, self).get_context_data(**kwargs)

		event_details = self.get_object()

		context['tab_list'] = LiveEventsTabs.objects.filter(liveevents=event_details).order_by('id')

		context['category_list'] = CourseCategory.objects.all().order_by('name')

		return context


class LiveEventsQueryCreateView(CommonMixin, CreateView):
	"""
	LiveEventsQuery Create View
	"""
	form_class = LiveEventEnquiryForm
	template_name = "events_enquiry.html"

	def get_success_url(self, **kwargs):
		events_details = LiveEvents.objects.filter(slug=self.kwargs['event_slug']).first()
		events_enquiry = LiveEventsEnquiry.objects.filter(id=self.object.id).first()
		# send_event_query_message_query_mail(contact=events_enquiry)
		messages.success(
	    	self.request, 'Send successfully! Our Team will get in touch with you.')
		return reverse('events:event_enquiry', kwargs={'event_slug': events_details.slug})

	def form_valid(self, form):
		events_details = LiveEvents.objects.filter(slug=self.kwargs['event_slug']).first()
		form.instance.liveevents = events_details
		return super(LiveEventsQueryCreateView, self).form_valid(form)

	def get_context_data(self, **kwargs):
		context = super(LiveEventsQueryCreateView, self).get_context_data(**kwargs)

		context['events_details'] = LiveEvents.objects.filter(slug=self.kwargs['event_slug']).first()

		context['contact_background'] = Contact_BackGround.objects.filter(
            sample__exact='Contact-Background').first()

		return context