"""
Technology (URLs)
"""
from django.conf.urls import url
from . import views

from django.views.decorators.cache import cache_page

app_name = 'live_events'

urlpatterns = [
	
	url(r'^$', cache_page(60*15)(views.LiveEventsListView.as_view()), name='all_events'),

	url(r'^(?P<event_slug>[\w-]+)/details/$',
        cache_page(60*15)(views.LiveEventsDetailsView.as_view()), name='event-details'),

	url(r'^enquiry/events/(?P<event_slug>[\w-]+)/$',
        views.LiveEventsQueryCreateView.as_view(), name='events_enquiry'),

]