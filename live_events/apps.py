from django.apps import AppConfig


class LiveEventsConfig(AppConfig):
    name = 'live_events'
