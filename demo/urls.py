"""demo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.contrib.auth import views as auth_views
from django.contrib.sitemaps.views import sitemap
from django.views.decorators.cache import cache_page


from app import views

from app.sitemaps import HomeViewSiteMap
from about_us.sitemaps import AboutUsSiteMap
from blog.sitemaps import BlogViewSiteMap, AllBlogCategoriesSiteMap, AllBlogSiteMap
from consultancy.sitemaps import ConsultancySiteMap
from courses.sitemaps import AllCourseCategorySiteMap, AllCourseSubCategorySiteMap, AllCoursesSiteMap
from JOBS.sitemaps import AllJobsSiteMap, SubmitResumeSiteMap
from contact_us.sitemaps import ContactUsSiteMap


sitemaps = {
    'home' : HomeViewSiteMap,
    'blog_list' : BlogViewSiteMap,
    'blog_categories' : AllBlogCategoriesSiteMap,
    'all_blogs' : AllBlogSiteMap,
    'consultancy' : ConsultancySiteMap,
    'course_category' : AllCourseCategorySiteMap,
    'course_sub_category' : AllCourseSubCategorySiteMap,
    'course' : AllCoursesSiteMap,
    'all-jobs' : AllJobsSiteMap,
    'resume' : SubmitResumeSiteMap,
    'contact' : ContactUsSiteMap
}

urlpatterns = [
	
	path('', views.index, name='index'),
    path('ankr/admin/', views.index_admin, name='index_admin'),
    path('ankr/student/', views.index_student, name='index_student'),
    path('privacy-policy/', views.privacy_view, name='privacy_policy'),
    path('terms-and-condition/', views.terms_view, name='terms_view'),
    path('daterangeupdate/', views.period_selected_update, name='dateupdate'),
    path('admin-shortcuts/', cache_page(60*15)(views.AdminShortcuts.as_view()), name='admin-shortcuts'),
    url(r'login_success/$', views.login_success, name='login_success'),
	path('admin/', admin.site.urls),
    url(r'^sitemap.xml/$', sitemap, {'sitemaps' : sitemaps} , name='sitemap'),
    url(r'^robots.txt', include('robots.urls')),
	url(r'^ckeditor/', include('ckeditor_uploader.urls')),

    ############# App Urls ################################

    url(r"^accounts/", include("accounts.urls", namespace="accounts")),
    url(r"^about-us/", include("about_us.urls", namespace="about_us")),
    url(r"^blogs/", include("blog.urls", namespace="blog")),
    url(r"^consultancy/", include("consultancy.urls", namespace="consultancy")),
    url(r"^contact/support/", include("contact_us.urls", namespace="contact_us")),
    url(r"^course/", include("courses.urls", namespace="courses")),
    url(r"^cart/", include("ecommerce_cart.urls", namespace="ecommerce_cart")),
    url(r"^faq/", include("FAQ.urls", namespace="faq")),
    url(r"^jobs/", include("JOBS.urls", namespace="jobs")),
    url(r"^events/", include("live_events.urls", namespace="events")),
    url(r"^technology/", include("technology.urls", namespace="technology")),
    url(r"^user/profile/", include("user_profile.urls", namespace="user_profile")),

    ############################ Forgot Password View ##############################

        # change password via email forgot password
    path('password-reset/', auth_views.PasswordResetView.as_view(template_name='registration/password_reset_form.html'),
         name='password_reset'),
    path('password-reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='registration/password_reset_done.html'),
         name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='registration/password_reset_confirm.html'),
         name='password_reset_confirm'),
    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(
             template_name='registration/password_reset_complete.html'),
         name='password_reset_complete'),
    

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
