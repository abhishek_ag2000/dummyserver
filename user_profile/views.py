from django.shortcuts import render, redirect
from django.db.models import Q
from django.urls import reverse
from django.views.generic import ListView, DetailView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages

from courses.models import UserCourses

from app.common import CommonAdminMixin

from .models import Profile
from .forms import ProfileForm
# Create your views here.

class StudentsListView(LoginRequiredMixin, CommonAdminMixin, ListView):
	"""
	Student List View
	"""
	model = Profile
	context_object_name = 'user_list'
	template_name = 'user_profile/student_list.html'
	paginate_by = 15

	def get(self, request, *args, **kwargs):
		# checking if user is user user
		if not request.user.is_superuser:
			messages.error(request, 'You have no permission to access the requested resource!')
			return redirect(reverse('index_student'))

		return super().get(request, *args, **kwargs)

	def get_queryset(self):
		student_list = Profile.objects.filter(user_type__exact='STUDENT').order_by('-id')
		return student_list


class ProfileDetailsView(LoginRequiredMixin, CommonAdminMixin, DetailView):
	"""
	Profile Details View
	"""
	context_object_name = 'profile'
	model = Profile
	template_name = "user_profile/profile.html"

	def get_object(self):
		profile = Profile.objects.filter(user=self.request.user).first()
		return profile

	def get_context_data(self, **kwargs):
		context = super(ProfileDetailsView, self).get_context_data(**kwargs)

		context['user_courses'] = UserCourses.objects.filter(user=self.request.user).order_by('-id')

		return context

class ProfileUpdateView(LoginRequiredMixin, CommonAdminMixin, UpdateView):
	"""
	Profile Update View
	"""
	model = Profile
	form_class = ProfileForm
	template_name = "user_profile/profile_update.html"

	def get_success_url(self, **kwargs):
		messages.success(self.request, 'Your Profile is Updated Successfully')
		return reverse('user_profile:profile_details')

	def get_object(self):
		profile = Profile.objects.filter(user=self.request.user).first()
		return profile



def user_search_autocomplete_ajax(request):
	"""
	Ajax Request View For Search User Autocomplete
	"""

	query = request.GET.get('search', None)

	if query:
		result = Profile.objects.filter(
			Q(user__is_active=True) ,
			Q(user__username__icontains=query) |
			Q(email__icontains=query) |
			Q(user_type__icontains=query)
			).exclude(user=request.user).order_by("user__username")
	else:
		result = Profile.objects.filter(user__is_active=True).exclude(user=request.user).order_by("user__username")

	page = request.GET.get('page', 1)

	paginator = Paginator(result, 15)

	try:
		users = paginator.page(page)
	except PageNotAnInteger:
		users = paginator.page(1)
	except EmptyPage:
		users = paginator.page(paginator.num_pages)

	context = {
		'user_list': users,  # user profile by page
		'last_query': query,
	}

	if request.is_ajax():
		html = render_to_string('user_profile/user_ajax_list.html',
	                    	context, request=request)

	data = {

		'html' : html,
		'query' : query,
	}

	return JsonResponse(data)


def student_search_autocomplete_ajax(request):
	"""
	Ajax Request View For Search Student Autocomplete
	"""

	query = request.GET.get('search', None)

	if query:
		result = Profile.objects.filter(
			Q(user__is_active=True) ,
			Q(user_type__exact='STUDENT') ,
			Q(user__username__icontains=query) |
			Q(email__icontains=query) |
			Q(user_type__icontains=query)
			).exclude(user=request.user).order_by("user__username")
	else:
		result = Profile.objects.filter(user__is_active=True, user_type__exact='STUDENT').exclude(user=request.user).order_by("user__username")

	page = request.GET.get('page', 1)

	paginator = Paginator(result, 15)

	try:
		users = paginator.page(page)
	except PageNotAnInteger:
		users = paginator.page(1)
	except EmptyPage:
		users = paginator.page(paginator.num_pages)

	context = {
		'user_list': users,  # user profile by page
		'last_query': query,
	}

	if request.is_ajax():
		html = render_to_string('user_profile/user_ajax_list.html',
	                    	context, request=request)

	data = {

		'html' : html,
		'query' : query,
	}

	return JsonResponse(data)