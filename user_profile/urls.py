"""
URLs
"""
from django.conf.urls import url
from django.urls import path
from . import views


app_name = 'user_profile'

urlpatterns = [

	
	url(r'^$', views.ProfileDetailsView.as_view(),
        name='profile_details'),

	url(r'^edit/$', views.ProfileUpdateView.as_view(),
        name='profile_edit'),
	
	url(r'^students/$', views.StudentsListView.as_view(),
        name='student_list'),

	url(r'^search/user/$', views.user_search_autocomplete_ajax,
        name='user_search'),

	url(r'^search/student/$', views.student_search_autocomplete_ajax,
        name='student_search'),

]