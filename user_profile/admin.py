from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import Profile
# Register your models here.

class ProfileAdmin(ImportExportModelAdmin):
	model = Profile
	list_display = ['user', 'email', 'user_type', ]


admin.site.register(Profile, ProfileAdmin)
