"""
Forms
"""
from django import forms
from django.conf import settings
from .models import Profile


class ProfileForm(forms.ModelForm):
	"""
	Profile Form
	"""

	def __init__(self, *args, **kwargs):
		super(ProfileForm, self).__init__(*args, **kwargs)

		for field in self.fields.values():
			field.widget.attrs = {"class": "form-control"}


	class Meta:
		model = Profile
		fields = ['full_name', 'email','permanent_address','city','state','country','phone_no','basic_info','image']