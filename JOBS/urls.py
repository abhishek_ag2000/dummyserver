"""
Urls
"""
from django.conf.urls import url
from . import views

app_name = 'JOBS'

urlpatterns = [
	url(r'^$', views.job_list, name='job_list'),
	url(r'^submit-application/$', views.ResumeSubmitView.as_view(), name='submit_resume'),
	url(r'^(?P<job_pk>\d+)/apply/$',
        views.JobApplyView.as_view(), name='apply_job'),
]