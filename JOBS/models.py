from PIL import Image
import sys
import os
from io import BytesIO
from django.db import models
from django.conf import settings
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.validators import URLValidator, RegexValidator

# Create your models here.

def file_size(value):  # add this to some file where you can import it from
	"""
	Function to validate file size
	"""
	MAX_UPLOAD_SIZE = 20971520
	if value.size > MAX_UPLOAD_SIZE:
		raise ValidationError('File too large. Size should not exceed 20 MB.')

class Skills(models.Model):
	"""
	Skills Model
	"""
	name = models.CharField(max_length=200)

	def __str__(self):
		return self.name


class JoinAnkr(models.Model):
	"""
	Join Ankr
	"""
	qualification_type = (
		('Graduate', 'Graduate'),
		('Post Graduate', 'Post Graduate'),
		('MBA', 'MBA'),
		('CA', 'CA'),
		('CS', 'CS'),
		('CMA', 'CMA'),
		('ICWA', 'ICWA'),
		('CFA', 'CFA'),
		('CPA', 'CPA'),
		('Semi Qualified CA', 'Semi Qualified CA'),
	)
	name = models.CharField(max_length=200)
	work_experience = models.CharField(
		max_length=200)
	phone_regex = RegexValidator(regex=r'^\+?1?\d{6,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
	phone_no = models.CharField(validators=[phone_regex], max_length=17, null=True) # validators should be a list
	email = models.EmailField(max_length=100, null=True)
	current_ctc = models.CharField(max_length=200, blank=True, null=True)
	expected_ctc = models.CharField(max_length=200)
	description = models.TextField(blank=True, null=True)
	qualification = models.CharField(
		max_length=150, choices=qualification_type, default='Graduate')
	open_to_relocate = models.BooleanField(default=False)
	open_to_work_secondment_basis = models.BooleanField(default=False)
	skills = models.ManyToManyField(
        Skills, related_name='user_skills_resume', blank=True)
	resume = models.FileField(upload_to="work/%Y/%m/%d", validators=[file_size], null=True)
	date = models.DateTimeField(auto_now_add=True)

	class Meta:
		app_label = 'JOBS'

	def __str__(self):
		return self.name

class Job(models.Model):
	"""
	Job Model
	"""
	job_title = models.CharField(max_length=50)
	location = models.TextField(null=True, blank=True)
	job_function = models.CharField(max_length=50)
	emp_type = (
		('Full-time', 'Full-time'),
		('Permanent', 'Permanent'),
		('Internship', 'Internship'),
		('Contract', 'Contract'),
		('Part-time', 'Part-time'),
	)
	employment_type = models.CharField(
		max_length=100, choices=emp_type, default='Choose')
	company_industry = models.CharField(max_length=50)
	seniorty_level = (
		('Entry level', 'Entry level'),
		('Associate', 'Associate'),
		('Internship', 'Internship'),
		('Mid-senior-level', 'Mid-senior-level'),
		('Director', 'Director'),
		('Executive', 'Executive'),
		('Not Available', 'Not Available'),
	)
	seniority = models.CharField(
		max_length=100, choices=seniorty_level, default='Choose', blank=True, null=True)
	job_desc = RichTextUploadingField(
		blank=True, null=True, config_name='special')
	skills = models.TextField(blank=True, null=True)
	date = models.DateTimeField(auto_now_add=True)
	job_image = models.ImageField(
		upload_to='job_pic', null=True, blank=True)
	applied_users = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name='applied_user', blank=True)

	class Meta:
		app_label = 'JOBS'

	def __str__(self):
		return self.job_title

	def save(self, *args, **kwargs):

		if self.job_image:
			temp_image = Image.open(self.job_image).convert('RGBA')
			output_stream = BytesIO()
			temp_resized_image = temp_image.resize((78, 84))
			temp_resized_image.save(output_stream, format='PNG', quality=60)
			output_stream.seek(0)
			self.job_image = InMemoryUploadedFile(
			    output_stream,
			    'ImageField',
			    "%s.png" % self.job_image.name.split(
			        '.')[0],  # need revision for file name
			    'image/png',
			    sys.getsizeof(output_stream),
		    	None)
       

		super(Job, self).save(*args, **kwargs)

class JobApply(models.Model):
	"""
	Job Apply Model
	"""
	qualification_type = (
		('Graduate', 'Graduate'),
		('Post Graduate', 'Post Graduate'),
		('MBA', 'MBA'),
		('CA', 'CA'),
		('CS', 'CS'),
		('CMA', 'CMA'),
		('ICWA', 'ICWA'),
		('CFA', 'CFA'),
		('CPA', 'CPA'),
		('Semi Qualified CA', 'Semi Qualified CA'),
	)
	YEARS = (
		('FRESHER', 'FRESHER'),
		('1-5 years', '1-5 years'),
		('5-10 years', '5-10 years'),
		('10-15 years', '10-15 years'),
		('More than 15 years', 'More than 15 years'),
	)
	job = models.ForeignKey(
        Job, on_delete=models.CASCADE, related_name='user_jobs')
	first_name = models.CharField(max_length=200)
	last_name = models.CharField(max_length=200)
	brief_about_you = models.TextField(blank=True, null=True)
	qualification = models.CharField(
		max_length=150, choices=qualification_type, default='Graduate')
	work_experience = models.CharField(
		max_length=150, choices=YEARS, default='FRESHER')
	current_location = models.CharField(max_length=200)
	open_to_relocate = models.BooleanField(default=False)
	open_to_work_secondment_basis = models.BooleanField(default=False)
	skills = models.ManyToManyField(
        Skills, related_name='user_skills', blank=True)
	resume = models.FileField(upload_to="resume/%Y/%m/%d", validators=[file_size], null=True)

	def __str__(self):
		return self.first_name