from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse

class AllJobsSiteMap(Sitemap):
	"""
	Sitemap For All Job's Url
	"""

	def items(self):
		return ['jobs:job_list']

	def location(self, item):
		return reverse(item)

class SubmitResumeSiteMap(Sitemap):
	"""
	Sitemap For Submit Resume Url
	"""

	def items(self):
		return ['jobs:submit_resume']

	def location(self, item):
		return reverse(item)