from django import forms

from .models import JoinAnkr, JobApply


class ApplicationForm(forms.ModelForm):
	"""
	Application Form
	"""
	class Meta:
		model = JoinAnkr
		fields = ('name', 'email','work_experience','phone_no','qualification','current_ctc','expected_ctc','description','resume','open_to_relocate','open_to_work_secondment_basis','skills',)

	def __init__(self, *args, **kwargs):
		super(ApplicationForm, self).__init__(*args, **kwargs)

		self.fields['name'].widget.attrs = {'placeholder': 'Name*'}
		self.fields['email'].widget.attrs = {'placeholder': 'Email*'}
		self.fields['work_experience'].widget.attrs = {'placeholder': 'Work Experience In Years*'}
		self.fields['phone_no'].widget.attrs = {'placeholder': 'Phone No*'}
		self.fields['current_ctc'].widget.attrs = {'placeholder': 'Your Current CTC'}
		self.fields['expected_ctc'].widget.attrs = {'placeholder': 'Expected CTC*'}
		self.fields['qualification'].widget.attrs = {'class': 'select2_demo_3 form-control', }
		self.fields['description'].widget.attrs = {'placeholder': 'Tell Something About You'}
		self.fields['resume'].widget.attrs = {'placeholder': 'Submit Resume'}
		self.fields['skills'].widget.attrs = {'class': 'select2_demo_2 form-control', 'multiple': 'multiple', }


class JobApplyForm(forms.ModelForm):
	"""
	Job Apply Form
	"""
	class Meta:
		model = JobApply
		fields = ('first_name','last_name','brief_about_you','qualification','work_experience','current_location','open_to_relocate','open_to_work_secondment_basis','skills','resume')

	def __init__(self, *args, **kwargs):
		super(JobApplyForm, self).__init__(*args, **kwargs)

		self.fields['first_name'].widget.attrs = {'placeholder': 'First Name*'}
		self.fields['last_name'].widget.attrs = {'placeholder': 'Last Name*'}
		self.fields['brief_about_you'].widget.attrs = {'placeholder': 'Tell Something About You'}
		self.fields['qualification'].widget.attrs = {'class': 'select2_demo_3 form-control', }
		self.fields['work_experience'].widget.attrs = {'class': 'select2_demo_4 form-control', }
		self.fields['current_location'].widget.attrs = {'placeholder': 'Current Location'}
		self.fields['skills'].widget.attrs = {'class': 'select2_demo_2 form-control', 'multiple': 'multiple', }