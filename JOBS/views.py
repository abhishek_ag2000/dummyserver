from django.shortcuts import render
from django.views.decorators.cache import cache_page
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import CreateView
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.urls import reverse

from app.common import CommonMixin

from .models import JoinAnkr, Job, JobApply, Skills
from .forms import ApplicationForm, JobApplyForm
# Create your views here.

@cache_page(60 * 15)
def job_list(request):
	"""
	Job List View
	"""
	jobs = Job.objects.all().order_by('-id')

	page = request.GET.get('page', 1)

	paginator = Paginator(jobs, 20)
	try:
		job_list = paginator.page(page)
	except PageNotAnInteger:
		job_list = paginator.page(1)
	except EmptyPage:
		job_list = paginator.page(paginator.num_pages)

	context = {
		'job_list' : job_list,
	}

	return CommonMixin.render(request, 'jobs_list.html',context)


class ResumeSubmitView(CommonMixin, CreateView):
	"""
	Resume Submit By Students View
	"""
	form_class = ApplicationForm
	template_name = 'application.html'

	def get_success_url(self, **kwargs):
		messages.success(
            self.request, 'Your Application For Joining Ankr is submitted successfully! Our Team will contact you soon')
		return reverse('jobs:submit_resume')


class JobApplyView(CommonMixin, CreateView):
	"""
	Specific Job Apply By Students View
	"""
	form_class = JobApplyForm
	template_name = 'apply_job.html'

	def get_success_url(self, **kwargs):
		messages.success(
            self.request, 'Your Request is successfully submitted| You will be notified soon')
		return reverse('jobs:job_list')

	def form_valid(self, form):
		job_details = Job.objects.filter(pk=self.kwargs['job_pk']).first()

		if not self.request.user in job_details.applied_users.all():
			job_details.applied_users.add(self.request.user)
			job_details.save()


		form.instance.job = job_details

		return super(JobApplyView, self).form_valid(form)

	def get_context_data(self, **kwargs):
		context = super(JobApplyView, self).get_context_data(**kwargs)

		context['job_details'] = Job.objects.filter(pk=self.kwargs['job_pk']).first()

		return context

