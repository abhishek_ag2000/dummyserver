from django.contrib import admin

from .models import Skills, JoinAnkr, Job, JobApply
# Register your models here.


admin.site.register(Skills)
admin.site.register(JoinAnkr)
admin.site.register(Job)
admin.site.register(JobApply)